package zw.co.afrosoft.pensions.payroll.branch;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/21/20
 * 8:25 PM
 **/

public interface BranchRepository extends JpaRepository<Branch, Long> {
    Optional<Branch> findByBranchCode(String branchCode);

    Optional<Branch> findByBranchName(String branchName);

}
