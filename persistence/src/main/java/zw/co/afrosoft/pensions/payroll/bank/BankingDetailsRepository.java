package zw.co.afrosoft.pensions.payroll.bank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author Romeo J
 * 8/22/20
 * 12:42 PM
 **/
public interface BankingDetailsRepository extends JpaRepository<BankingDetails, Long> {
    BankingDetails findByAccountNumber(String accountNumber);

    Page<BankingDetails> findAll(Pageable pageable);

}
