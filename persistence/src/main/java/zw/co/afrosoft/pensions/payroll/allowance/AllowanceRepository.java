package zw.co.afrosoft.pensions.payroll.allowance;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:32 AM
 **/
public interface AllowanceRepository extends JpaRepository<Allowance, Long> {

    Optional<Allowance> findByIsActive(Boolean isActive);

    Optional<Allowance> findByIsTaxable(Boolean isTaxable);

    Optional<Allowance> findByAllowanceName(String allowanceName);

}
