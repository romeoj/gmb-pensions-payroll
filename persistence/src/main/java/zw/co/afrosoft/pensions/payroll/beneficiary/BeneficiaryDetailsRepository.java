package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:43 PM
 **/
public interface BeneficiaryDetailsRepository extends JpaRepository<BeneficiaryDetails, Long> {
}
