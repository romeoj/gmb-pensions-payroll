package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:42 PM
 **/
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long> {
}
