package zw.co.afrosoft.pensions.payroll.remuneration;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author Romeo J
 * 8/24/20
 * 8:28 PM
 **/

public interface RemunerationRepository extends JpaRepository<Remuneration, Long> {
}
