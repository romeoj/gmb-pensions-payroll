package zw.co.afrosoft.pensions.payroll.bank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:02 PM
 **/

public interface BankRepository extends JpaRepository<Bank,Long> {
    Optional<Bank> findBankByBankCode(String bankCode);

    Optional<Bank> findBankByBankName(String bankName);

    Page<Bank> findAll(Pageable pageable);

}
