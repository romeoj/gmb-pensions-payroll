package zw.co.afrosoft.pensions.payroll.deduction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/24/20
 * 11:17 AM
 **/

public interface DeductionRepository extends JpaRepository<Deduction, Long> {
    Page<Deduction> findAll(Pageable pageable);

    Optional<Deduction> findByIsActive(Boolean isActive);

    Optional<Deduction> findByIsFixed(Boolean isFixed);

    Optional<Deduction> findByDeductionName(String deductionName);
}
