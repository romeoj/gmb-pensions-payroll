package zw.co.afrosoft.pensions.payroll.salary;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.afrosoft.pensions.payroll.enums.PaymentStatus;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/24/20
 * 2:08 PM
 **/

public interface SalaryRepository extends JpaRepository<Salary, Long> {
    Optional<Salary> findByPaymentStatus(PaymentStatus paymentStatus);
}
