package zw.co.afrosoft.pensions.payroll.pensioner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.afrosoft.pensions.payroll.enums.PensionerStatus;

import java.util.Optional;

/**
 * @Author Romeo J
 * 8/22/20
 * 11:53 AM
 **/
public interface PensionerRepository extends JpaRepository<Pensioner, Long> {
    Optional<Pensioner> findByPensionerNumber(String pensionerNumber);

    Page<Pensioner> findAll(Pageable pageable);

    Optional<Pensioner> findByPensionerStatus(PensionerStatus pensionerStatus);
}
