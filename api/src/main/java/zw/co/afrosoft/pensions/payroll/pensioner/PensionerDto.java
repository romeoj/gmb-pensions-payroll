package zw.co.afrosoft.pensions.payroll.pensioner;

import zw.co.afrosoft.pensions.payroll.enums.Category;
import zw.co.afrosoft.pensions.payroll.enums.PensionerStatus;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 6:03 PM
 **/
public class PensionerDto {
    private Long id;
    private String pensionerNumber;
    private PensionerStatus pensionerStatus;
    private Category category;

    public PensionerDto(Long id, String pensionerNumber, PensionerStatus pensionerStatus, Category category) {
        this.id = id;
        this.pensionerNumber = pensionerNumber;
        this.pensionerStatus = pensionerStatus;
        this.category = category;
    }

    public static PensionerDto of(Pensioner pensioner){
        if (isNull(pensioner)){
            return null;
        }
        return new PensionerDto(pensioner.getId(), pensioner.getPensionerNumber(), pensioner.getPensionerStatus(),
                pensioner.getCategory());
    }

    public static List<PensionerDto> of(List<Pensioner> pensioners){
        if (isNull(pensioners)){
            return null;
        }
        return pensioners.stream().map(PensionerDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPensionerNumber() {
        return pensionerNumber;
    }

    public void setPensionerNumber(String pensionerNumber) {
        this.pensionerNumber = pensionerNumber;
    }

    public PensionerStatus getPensionerStatus() {
        return pensionerStatus;
    }

    public void setPensionerStatus(PensionerStatus pensionerStatus) {
        this.pensionerStatus = pensionerStatus;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
