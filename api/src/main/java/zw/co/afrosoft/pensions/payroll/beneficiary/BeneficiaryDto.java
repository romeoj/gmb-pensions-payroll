package zw.co.afrosoft.pensions.payroll.beneficiary;

import zw.co.afrosoft.pensions.payroll.enums.Relationship;
import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/23/20
 * 8:35 PM
 **/
public class BeneficiaryDto {
    private Long id;
    private Relationship relationship;
    private Boolean isNominee;
    private Boolean isBeneficiary;
    private Pensioner pensioner;

    public BeneficiaryDto(Long id, Relationship relationship, Boolean isNominee,
                          Boolean isBeneficiary, Pensioner pensioner) {
        this.id = id;
        this.relationship = relationship;
        this.isNominee = isNominee;
        this.isBeneficiary = isBeneficiary;
        this.pensioner = pensioner;
    }
    public static BeneficiaryDto of(Beneficiary beneficiary){
        if (isNull(beneficiary)){
            return null;
        }
        return new BeneficiaryDto(beneficiary.getId(), beneficiary.getRelationship(), beneficiary.isNominee(),
                beneficiary.isBeneficiary(), beneficiary.getPensioner());
    }
    public static List<BeneficiaryDto> of(List<Beneficiary> beneficiaries){
        if (isNull(beneficiaries)){
            return null;
        }
        return beneficiaries.stream().map(BeneficiaryDto::of).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public void setRelationship(Relationship relationship) {
        this.relationship = relationship;
    }

    public Boolean getNominee() {
        return isNominee;
    }

    public void setNominee(Boolean isNominee) {
        this.isNominee = isNominee;
    }

    public Boolean getBeneficiary() {
        return isBeneficiary;
    }

    public void setBeneficiary(Boolean isBeneficiary) {
        this.isBeneficiary = isBeneficiary;
    }

    public Pensioner getPensioner() {
        return pensioner;
    }

    public void setPensioner(Pensioner pensioner) {
        this.pensioner = pensioner;
    }
}
