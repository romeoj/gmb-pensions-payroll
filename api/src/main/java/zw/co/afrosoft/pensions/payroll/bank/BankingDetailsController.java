package zw.co.afrosoft.pensions.payroll.bank;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.bank.BankingDetailsDto.of;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.FAILED;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/22/20
 * 3:23 PM
 **/

@RestController
@RequestMapping("v1/banking-details")
public class BankingDetailsController {
    private static final Logger LOGGER = getLogger(BankingDetailsController.class);
    private final BankingDetailsService bankingDetailsService;

    public BankingDetailsController(BankingDetailsService bankingDetailsService) {
        this.bankingDetailsService = bankingDetailsService;
    }
    @PostMapping
    public AppResponse<BankingDetailsDto> createBankingDetails(@Valid @RequestBody BankingDetailsRequest request){
        requireNonNull(request, "banking details request cannot be null");
        LOGGER.info("Create banking details request, request = {}", request);
        BankingDetails bankingDetails = bankingDetailsService.createBankingDetails(request);
        if (isNull(bankingDetails)){
            return new AppResponse<>(CREATE_BANKING_DETAILS_FAILURE, FAILED);
        }
        return new AppResponse<>(of(bankingDetails), CREATE_BANKING_DETAILS_SUCCESS, SUCCESS);
    }
    @GetMapping("bankingDetailsId")
    public AppResponse<BankingDetailsDto> getBankingDetails(@PathVariable Long bankingDetailsId){
        requireNonNull(bankingDetailsId, "banking details id cannot be null");
        LOGGER.info("Retrieving banking details by id, bankingDetailsId = {}", bankingDetailsId);
        BankingDetails bankingDetails = bankingDetailsService.findBankingDetailsById(bankingDetailsId);
        return new AppResponse<>(of(bankingDetails), BANKING_DETAILS_SEARCH_SUCCESS,SUCCESS);
    }
    @GetMapping("account-number/{accountNumber}")
    public AppResponse<BankingDetailsDto> getBankingDetailsByAccountNumber(@PathVariable String accountNumber){
        requireNonNull(accountNumber, "account number cannot be null");
        LOGGER.info("Retrieving banking details by account number, accountNumber = {}", accountNumber);
        BankingDetails bankingDetails = bankingDetailsService.findBankingDetailsByAccountNumber(accountNumber);
        return new AppResponse<>(of(bankingDetails), BANKING_DETAILS_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<BankingDetailsDto> getAllBankingDetails(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all banking details");
        Page<BankingDetails> bankingDetailsPage = bankingDetailsService.findAllBankingDetails(pageable);
        return new PageImpl<>(of(bankingDetailsPage.getContent()), bankingDetailsPage.getPageable(),
                bankingDetailsPage.getTotalElements());
    }
    @PutMapping("{bankingDetailsId}")
    public AppResponse<BankingDetailsDto> updateBankingDetails(@PathVariable Long bankingDetailsId,
                                                               @Valid @RequestBody BankingDetailsUpdateRequest updateRequest){
        requireNonNull(bankingDetailsId, "banking details id cannot be null");
        requireNonNull(updateRequest, "banking details update request cannot be null");
        BankingDetails bankingDetails = bankingDetailsService.editBankingDetails(bankingDetailsId, updateRequest);
        return new AppResponse<>(of(bankingDetails), UPDATE_BANKING_DETAILS_SUCCESS, SUCCESS);
    }
}
