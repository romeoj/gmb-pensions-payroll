package zw.co.afrosoft.pensions.payroll.salary;

import zw.co.afrosoft.pensions.payroll.allowance.Allowance;
import zw.co.afrosoft.pensions.payroll.bank.BankingDetails;
import zw.co.afrosoft.pensions.payroll.deduction.Deduction;
import zw.co.afrosoft.pensions.payroll.enums.PaymentStatus;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 4:39 PM
 **/

public class SalaryDto {
    private Long id;
    private Double basicAmount;
    private Double payableAmount;
    private Deduction deduction;
    private Allowance allowance;
    private BankingDetails bankingDetails;
    private Boolean isAllowance;
    private PaymentStatus paymentStatus;

    public SalaryDto(Long id, Double basicAmount, Double payableAmount, Deduction deduction, Allowance allowance,
                     BankingDetails bankingDetails, Boolean isAllowance, PaymentStatus paymentStatus) {
        this.id = id;
        this.basicAmount = basicAmount;
        this.payableAmount = payableAmount;
        this.deduction = deduction;
        this.allowance = allowance;
        this.bankingDetails = bankingDetails;
        this.isAllowance = isAllowance;
        this.paymentStatus = paymentStatus;
    }

    public static SalaryDto of(Salary salary){
        if (isNull(salary)){
            return null;
        }
        return new SalaryDto(salary.getId(), salary.getBasicAmount(), salary.getPayableAmount(), salary.getDeduction(),
                salary.getAllowance(), salary.getBankingDetails(), salary.getAllowanceStatus(), salary.getPaymentStatus());
    }

    public static List<SalaryDto> of(List<Salary> salaries){
        if (isNull(salaries)){
            return null;
        }
        return salaries.stream().map(SalaryDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBasicAmount() {
        return basicAmount;
    }

    public void setBasicAmount(Double basicAmount) {
        this.basicAmount = basicAmount;
    }

    public Double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(Double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }

    public Boolean isAllowance(){
        return isAllowance;
    }
    public void setIsAllowance(Boolean isAllowance) {
        this.isAllowance = isAllowance;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Allowance getAllowance() {
        return allowance;
    }

    public void setAllowance(Allowance allowance) {
        this.allowance = allowance;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }
}
