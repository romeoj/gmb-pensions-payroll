package zw.co.afrosoft.pensions.payroll.report;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author Romeo J
 * 8/25/20
 * 12:43 PM
 **/

@RestController
@RequestMapping("v1/reports")
public class ReportController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
    private final ReportService reportService;
    private Map<String, Object> params = new HashMap<>();
    private static final String DATE_FROM = "From_Date";
    private static final String DATE_TO = "To_Date";

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/medicalAid")
    public void medicalAid(@RequestParam String fromDate, @RequestParam String toDate, HttpServletResponse response)
            throws IOException, JRException, SQLException {
        LOGGER.info("Generating MedicalAid report");
        Date sqlFromDate = java.sql.Date.valueOf(fromDate);
        Date sqlToDate = java.sql.Date.valueOf(toDate);
        params.put(DATE_FROM, sqlFromDate);
        params.put(DATE_TO, sqlToDate);
        reportService.generateReport("MedicalAid", params, response);
    }

    @GetMapping("/payAsYouEarn")
    public void payAsYouEarn(@RequestParam String fromDate, @RequestParam String toDate, HttpServletResponse response)
            throws IOException, JRException, SQLException {
        LOGGER.info("Generating PAYE report");
        Date sqlFromDate = java.sql.Date.valueOf(fromDate);
        Date sqlToDate = java.sql.Date.valueOf(toDate);
        params.put(DATE_FROM, sqlFromDate);
        params.put(DATE_TO, sqlToDate);
        reportService.generateReport("PAYE", params, response);
    }

    @GetMapping("/payments")
    public void payments(@RequestParam String fromDate, @RequestParam String toDate, HttpServletResponse response)
            throws IOException, JRException, SQLException {
        LOGGER.info("Generating Payments report");
        java.sql.Date sqlDate = java.sql.Date.valueOf(fromDate);
        Date sqlFromDate =  sqlDate;
        Date sqlToDate = java.sql.Date.valueOf(toDate);
        params.put(DATE_FROM, sqlFromDate);
        params.put(DATE_TO, sqlToDate);
        reportService.generateReport("Payments", params, response);
    }

    @GetMapping("/payrollControl")
    public void payrollControl(@RequestParam String fromDate, @RequestParam String toDate, HttpServletResponse response)
            throws IOException, JRException, SQLException {
        LOGGER.info("Generating PayrollControl report");
        Date sqlFromDate = java.sql.Date.valueOf(fromDate);
        Date sqlToDate = java.sql.Date.valueOf(toDate);
        params.put(DATE_FROM, sqlFromDate);
        params.put(DATE_TO, sqlToDate);
        reportService.generateReport("PayrollControl", params, response);
    }

    @GetMapping("/payslip")
    public void payslip(@RequestParam String fromDate, @RequestParam String toDate, HttpServletResponse response)
            throws IOException, JRException, SQLException {
        LOGGER.info("Generating Payslip report");
        Date sqlFromDate = java.sql.Date.valueOf(fromDate);
        Date sqlToDate = java.sql.Date.valueOf(toDate);
        params.put(DATE_FROM, sqlFromDate);
        params.put(DATE_TO, sqlToDate);
        reportService.generateReport("Payslip", params, response);
    }

}
