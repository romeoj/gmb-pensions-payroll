package zw.co.afrosoft.pensions.payroll.remuneration;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.CREATE_REMUNERATION_SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.REMUNERATION_SEARCH_SUCCESS;
import static zw.co.afrosoft.pensions.payroll.remuneration.RemunerationDto.of;

/**
 * @Author Romeo J
 * 8/24/20
 * 8:49 PM
 **/

@RestController
@RequestMapping("v1/remuneration")
public class RemunerationController {
    private static final Logger LOGGER = getLogger(RemunerationController.class);
    private final RemunerationService remunerationService;

    public RemunerationController(RemunerationService remunerationService) {
        this.remunerationService = remunerationService;
    }

    @PostMapping
    public AppResponse<RemunerationDto> createRemuneration(@Valid @RequestBody RemunerationRequest request){
        requireNonNull(request, "remuneration request cannot be null");
        LOGGER.info("Create remuneration, request = {}", request);
        Remuneration remuneration = remunerationService.createRemuneration(request);
        return new AppResponse<>(of(remuneration), CREATE_REMUNERATION_SUCCESS, SUCCESS);
    }
    @GetMapping("{remunerationId}")
    public AppResponse<RemunerationDto> getRemuneration(@PathVariable Long remunerationId){
        requireNonNull(remunerationId, "remuneration id cannot be null");
        LOGGER.info("Retrieving remuneration by id, remunerationId = {}", remunerationId);
        Remuneration remuneration = remunerationService.findRemunerationById(remunerationId);
        return new AppResponse<>(of(remuneration), REMUNERATION_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<RemunerationDto> getAllRemunerations(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all remunerations");
        Page<Remuneration> remunerationPage = remunerationService.findAllRemunerations(pageable);
        return new PageImpl<>(of(remunerationPage.getContent()), remunerationPage.getPageable(),
                remunerationPage.getTotalElements());
    }
}
