package zw.co.afrosoft.pensions.payroll.beneficiary;

import zw.co.afrosoft.pensions.payroll.bank.BankingDetails;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 9:44 AM
 **/
public class BeneficiaryDetailsDto {
    private Long id;
    private String fullName;
    private LocalDate dateOfBirth;
    private String nationalId;
    private BankingDetails bankingDetails;
    private Beneficiary beneficiary;

    public BeneficiaryDetailsDto(Long id, String fullName, LocalDate dateOfBirth, String nationalId,
                                 BankingDetails bankingDetails, Beneficiary beneficiary) {
        this.id = id;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.nationalId = nationalId;
        this.bankingDetails = bankingDetails;
        this.beneficiary = beneficiary;
    }
    public static BeneficiaryDetailsDto of(BeneficiaryDetails beneficiaryDetails){
        if (isNull(beneficiaryDetails)){
            return null;
        }
        return new BeneficiaryDetailsDto(beneficiaryDetails.getId(), beneficiaryDetails.getFullName(),
                beneficiaryDetails.getDateOfBirth(), beneficiaryDetails.getNationalId(), beneficiaryDetails.getBankingDetails(),
                beneficiaryDetails.getBeneficiary());
    }
    public static List<BeneficiaryDetailsDto> of(List<BeneficiaryDetails> beneficiaryDetails){
        if (isNull(beneficiaryDetails)){
            return null;
        }
        return beneficiaryDetails.stream().map(BeneficiaryDetailsDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }
}
