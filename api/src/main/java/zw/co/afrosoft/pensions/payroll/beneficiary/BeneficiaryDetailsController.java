package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.beneficiary.BeneficiaryDetailsDto.of;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.FAILED;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/24/20
 * 9:44 AM
 **/

@RestController
@RequestMapping("v1/beneficiary-details")
public class BeneficiaryDetailsController {
    private static final Logger LOGGER = getLogger(BeneficiaryDetailsController.class);
    private final BeneficiaryDetailsService beneficiaryDetailsService;

    public BeneficiaryDetailsController(BeneficiaryDetailsService beneficiaryDetailsService) {
        this.beneficiaryDetailsService = beneficiaryDetailsService;
    }

    @PostMapping
    public AppResponse<BeneficiaryDetailsDto> createBeneficiaryDetails(@Valid @RequestBody BeneficiaryDetailsRequest request){
        requireNonNull(request, "beneficiary details create request cannot be null");
        LOGGER.info("Create beneficiary details, request = {}", request);
        BeneficiaryDetails beneficiaryDetails = beneficiaryDetailsService.createBeneficiaryDetails(request);
        if (isNull(beneficiaryDetails)){
            return new AppResponse<>(CREATE_BENEFICIARY_DETAILS_FAILURE, FAILED);
        }
        return new AppResponse<>(of(beneficiaryDetails), CREATE_BENEFICIARY_DETAILS_SUCCESS, SUCCESS);
    }
    @GetMapping("{beneficiaryDetailsId}")
    public AppResponse<BeneficiaryDetailsDto> getBeneficiaryDetails(@PathVariable Long beneficiaryDetailsId){
        requireNonNull(beneficiaryDetailsId, "beneficiary details id cannot be null");
        LOGGER.info("Retrieving beneficiary details by id, beneficiaryDetailsId = {}", beneficiaryDetailsId);
        BeneficiaryDetails beneficiaryDetails = beneficiaryDetailsService.findBeneficiaryDetailsById(beneficiaryDetailsId);
        return new AppResponse<>(of(beneficiaryDetails), BENEFICIARY_DETAILS_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<BeneficiaryDetailsDto> getAllBeneficiaryDetails(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all beneficiary details");
        Page<BeneficiaryDetails> beneficiaryDetailsPage = beneficiaryDetailsService.findAllBeneficiaryDetails(pageable);
        return new PageImpl<>(of(beneficiaryDetailsPage.getContent()), beneficiaryDetailsPage.getPageable(),
                beneficiaryDetailsPage.getTotalElements());
    }
    @PutMapping("{beneficiaryDetailsId}")
    public AppResponse<BeneficiaryDetailsDto> updateBeneficiaryDetails(@PathVariable Long beneficiaryDetailsId, @Valid
                                                                       @RequestBody BeneficiaryDetailsRequest request){
        requireNonNull(beneficiaryDetailsId, "beneficiary details id cannot be null");
        requireNonNull(request, "beneficiary details request cannot be null");
        LOGGER.info("Updating beneficiary details, beneficiaryDetailsId = {}, request = {}", beneficiaryDetailsId, request);
        BeneficiaryDetails beneficiaryDetails = beneficiaryDetailsService.editBeneficiaryDetails(beneficiaryDetailsId, request);
        return new AppResponse<>(of(beneficiaryDetails), BENEFICIARY_DETAILS_UPDATE_SUCCESS, SUCCESS);
    }
}
