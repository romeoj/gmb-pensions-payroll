package zw.co.afrosoft.pensions.payroll.pensioner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;
import static zw.co.afrosoft.pensions.payroll.pensioner.PensionerDto.of;

/**
 * @Author Romeo J
 * 8/22/20
 * 6:03 PM
 **/

@RestController
@RequestMapping("v1/pensioner")
public class PensionerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PensionerController.class);
    private final PensionerService pensionerService;

    public PensionerController(PensionerService pensionerService) {
        this.pensionerService = pensionerService;
    }

    @PostMapping
    public AppResponse<PensionerDto> createPensioner(@Valid @RequestBody PensionerCreateRequest createRequest){
        requireNonNull(createRequest, "pensioner create request cannot be null");
        LOGGER.info("Create pensioner, createPensioner = {}", createRequest);
        Pensioner pensioner = pensionerService.createPensioner(createRequest);
        return new AppResponse<>(of(pensioner), CREATE_PENSIONER_SUCCESS, SUCCESS);
    }
    @GetMapping("{pensionerId}")
    public AppResponse<PensionerDto> getPensioner(@PathVariable Long pensionerId){
        requireNonNull(pensionerId, "pensioner id cannot be null");
        LOGGER.info("Retrieving pensioner by id, pensionerId = {}", pensionerId);
        Pensioner pensioner = pensionerService.findPensionerById(pensionerId);
        return new AppResponse<>(of(pensioner), PENSIONER_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("pensioner-number/{pensionerNumber}")
    public AppResponse<PensionerDto> getPensionerByPensionerNumber(@PathVariable String pensionerNumber){
        requireNonNull(pensionerNumber, "pensioner number cannot be null");
        LOGGER.info("Retrieving pensioner by pensioner number, pensionerNumber = {}", pensionerNumber);
        Pensioner pensioner = pensionerService.findPensionerByPensionerNumber(pensionerNumber);
        return  new AppResponse<>(of(pensioner), PENSIONER_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("status/{pensionerStatus}")
    public AppResponse<PensionerDto> getPensionerByPensionerStatus(@PathVariable String pensionerStatus){
        requireNonNull(pensionerStatus, "pensioner status cannot be null");
        LOGGER.info("Retrieving pensioner by pensioner status, pensionerStatus", pensionerStatus);
        Pensioner pensioner = pensionerService.findPensionerByPensionerStatus(pensionerStatus);
        return new AppResponse<>(of(pensioner), PENSIONER_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<PensionerDto> getAllPensioners(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all pensioners");
        Page<Pensioner> pensionerPage = pensionerService.findAllPensioner(pageable);
        return new PageImpl<>(of(pensionerPage.getContent()), pensionerPage.getPageable(),
                pensionerPage.getTotalElements());
    }
    @PutMapping("{pensionerId}")
    public AppResponse<PensionerDto> updatePensioner(@PathVariable Long pensionerId,
                                                     @Valid @RequestBody PensionerUpdateRequest updateRequest){
        requireNonNull(pensionerId, "pensioner id cannot be null");
        requireNonNull(updateRequest, "pensioner update request cannot be null");
        LOGGER.info("Updating pensioner, pensionerId = {}, updateRequest = {}", pensionerId, updateRequest);
        Pensioner pensioner = pensionerService.editPensioner(pensionerId, updateRequest);
        return new AppResponse<>(of(pensioner), UPDATE_PENSIONER_SUCCESS, SUCCESS);
    }
}
