package zw.co.afrosoft.pensions.payroll.allowance;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.allowance.AllowanceDto.of;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.FAILED;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/22/20
 * 10:18 AM
 **/

@RestController
@RequestMapping("v1/allowance")
public class AllowanceController {
    private static final Logger LOGGER = getLogger(AllowanceController.class);
    private final AllowanceService allowanceService;

    public AllowanceController(AllowanceService allowanceService) {
        this.allowanceService = allowanceService;
    }
    @PostMapping
    public AppResponse<AllowanceDto> createAllowance(@Valid @RequestBody AllowanceCreateRequest request){
        requireNonNull(request, "Allowance create request cannot be null");
        LOGGER.info("Create Allowance, request = {}", request);
        Allowance allowance = allowanceService.createAllowance(request);
        if (isNull(allowance)){
            return new AppResponse<>(CREATE_ALLOWANCE_FAILURE, FAILED);
        }
        return new AppResponse<>(of(allowance), CREATE_ALLOWANCE_SUCCESS, SUCCESS);
    }
    @GetMapping("{allowanceId}")
    public AppResponse<AllowanceDto> getAllowance(@PathVariable Long allowanceId){
        requireNonNull(allowanceId, "allowance id cannot be null");
        LOGGER.info("Retrieving allowance by id, allowanceId - {}", allowanceId);
        Allowance allowance =allowanceService.findAllowanceById(allowanceId);
        return new AppResponse<>(of(allowance), ALLOWANCE_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("active/{active}")
    public AppResponse<AllowanceDto> getAllowanceByActiveStatus(@PathVariable Boolean active){
        requireNonNull(active, "allowance active cannot be null");
        LOGGER.info("Retrieving allowance by active, active = {}", active);
        Allowance allowance = allowanceService.findByIsActive(active);
        return new AppResponse<>(of(allowance), ALLOWANCE_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("taxable/{taxable}")
    public AppResponse<AllowanceDto> getAllowanceByTaxStatus(@PathVariable Boolean taxable){
        requireNonNull(taxable, "allowance taxable cannot be null");
        LOGGER.info("Retrieving allowance by taxable, taxable = {}", taxable);
        Allowance allowance = allowanceService.findByIsTaxable(taxable);
        return new AppResponse<>(of(allowance), ALLOWANCE_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("name/{allowanceName}")
    public AppResponse<AllowanceDto> getAllowanceByName(@PathVariable String allowanceName){
        requireNonNull(allowanceName, "allowance name cannot be null");
        LOGGER.info("Retrieving allowance by name, allowanceName = {}", allowanceName);
        Allowance allowance = allowanceService.findByAllowanceName(allowanceName);
        return new AppResponse<>(AllowanceDto.of(allowance), ALLOWANCE_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<AllowanceDto> getAllAllowances(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all allowances");
        Page<Allowance> allowancePage = allowanceService.findAllAllowances(pageable);
        return new PageImpl<>(of(allowancePage.getContent()), allowancePage.getPageable(),
                allowancePage.getTotalElements());
    }
    @PutMapping("{allowanceId}")
    public AppResponse<AllowanceDto> updateAllowance(@PathVariable Long allowanceId,
                                                     @RequestBody AllowanceUpdateRequest updateRequest){
        requireNonNull(allowanceId, "allowance id cannot be null");
        requireNonNull(updateRequest, "allowance update request cannot be null");
        LOGGER.info("Updating allowance, allowanceId = {}, updateRequest = {}", allowanceId, updateRequest);
        Allowance allowance = allowanceService.editAllowance(allowanceId, updateRequest);
        return new AppResponse<>(of(allowance), ALLOWANCE_UPDATE_SUCCESS, SUCCESS);
    }
}
