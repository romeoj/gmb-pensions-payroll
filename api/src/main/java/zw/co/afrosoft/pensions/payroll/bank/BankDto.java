package zw.co.afrosoft.pensions.payroll.bank;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:14 PM
 **/
public class BankDto {
    private Long id;
    private String bankName;
    private String bankCode;
    private Boolean active;

    private BankDto(Long id, String bankName, String bankCode, Boolean active) {
        this.id = id;
        this.bankName = bankName;
        this.bankCode = bankCode;
        this.active = active;
    }

    public static BankDto of(Bank bank){
        if (isNull(bank)){
            return null;
        }
        return new BankDto(bank.getId(), bank.getBankName(),bank.getBankCode(), bank.isActive());
    }
    public static List<BankDto> of(List<Bank> banks){
        if(isNull(banks)){
            return null;
        }
        return banks.stream().map(BankDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public Boolean isActive() {
        return active;
    }
}
