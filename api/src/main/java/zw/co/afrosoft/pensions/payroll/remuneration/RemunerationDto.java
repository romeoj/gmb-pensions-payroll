package zw.co.afrosoft.pensions.payroll.remuneration;

import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;
import zw.co.afrosoft.pensions.payroll.salary.Salary;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 8:49 PM
 **/

public class RemunerationDto {
    private Long id;
    private Pensioner pensioner;
    private Salary salary;

    public RemunerationDto(Long id, Pensioner pensioner, Salary salary) {
        this.id = id;
        this.pensioner = pensioner;
        this.salary = salary;
    }
    public static RemunerationDto of(Remuneration remuneration){
        if (isNull(remuneration)){
            return null;
        }
        return new RemunerationDto(remuneration.getId(), remuneration.getPensioner(), remuneration.getSalary());
    }

    public static List<RemunerationDto> of(List<Remuneration> remunerations){
        if (isNull(remunerations)){
            return null;
        }
        return remunerations.stream().map(RemunerationDto::of).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pensioner getPensioner() {
        return pensioner;
    }

    public void setPensioner(Pensioner pensioner) {
        this.pensioner = pensioner;
    }

    public Salary getSalary() {
        return salary;
    }

    public void setSalary(Salary salary) {
        this.salary = salary;
    }
}
