package zw.co.afrosoft.pensions.payroll.branch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.branch.BranchDto.of;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/21/20
 * 9:30 PM
 **/

@RestController
@RequestMapping("v1/branch")
public class BranchController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BranchController.class);
    private final BranchService branchService;

    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    @PostMapping
    public AppResponse<BranchDto> createBranch(@Valid @RequestBody BranchRequest branchRequest){
        requireNonNull(branchRequest, "branch request cannot be null");
        LOGGER.info("Creating branch, branchRequest = {}", branchRequest);
        Branch branch = branchService.createBranch(branchRequest);
        return new AppResponse<>(of(branch), CREATE_BRANCH_SUCCESS, SUCCESS);
    }
    @GetMapping("{branchId}")
    public AppResponse<BranchDto> getBranch(@PathVariable Long branchId){
        requireNonNull(branchId, "branch id cannot be null");
        LOGGER.info("Retrieving branch by id, branchId = {}", branchId);
        Branch branch = branchService.findBranchById(branchId);
        return new AppResponse<>(of(branch), BRANCH_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("name/{branchName}")
    public AppResponse<BranchDto> getBranchByName(@PathVariable String branchName){
        requireNonNull(branchName, "branch name cannot be null");
        LOGGER.info("Retrieving branch by name, branchName = {}", branchName);
        Branch branch = branchService.findBranchByBranchName(branchName);
        return new
                AppResponse<>(of(branch), BRANCH_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("code/{branchCode}")
    public AppResponse<BranchDto> getBranchByCode(@PathVariable String branchCode){
        requireNonNull(branchCode, "branch code cannot be null");
        LOGGER.info("Retrieving branch by code, branchCode = {}", branchCode);
        Branch branch = branchService.findBranchByBranchCode(branchCode);
        return new AppResponse<>(of(branch), BRANCH_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<BranchDto> getAllBranches(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all branches");
        Page<Branch> branchPage = branchService.findAllBranches(pageable);
        return new PageImpl<>(of(branchPage.getContent()), branchPage.getPageable(),
                branchPage.getTotalElements());
    }
    @PutMapping("{branchId}")
    public AppResponse<BranchDto> updateBranch(@PathVariable Long branchId, @RequestBody
            BranchUpdateRequest updateRequest){
        requireNonNull(branchId, "branch id cannot be null");
        requireNonNull(updateRequest, "branch update request cannot be null");
        LOGGER.info("Updating branch, branchId = {}, updateRequest = {}", branchId, updateRequest);
        Branch branch = branchService.editBranch(branchId, updateRequest);
        return new AppResponse<>(of(branch), BRANCH_UPDATE_SUCCESS, SUCCESS);
    }
}
