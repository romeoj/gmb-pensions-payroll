package zw.co.afrosoft.pensions.payroll.salary;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;
import static zw.co.afrosoft.pensions.payroll.salary.SalaryDto.of;

/**
 * @Author Romeo J
 * 8/24/20
 * 4:39 PM
 **/

@RestController
@RequestMapping("v1/salary")
public class SalaryController {
    private static final Logger LOGGER = getLogger(SalaryController.class);
    private final SalaryService salaryService;

    public SalaryController(SalaryService salaryService) {
        this.salaryService = salaryService;
    }
    @PostMapping
    public AppResponse<SalaryDto> createSalary(@Valid @RequestBody SalaryRequest salaryRequest){
        requireNonNull(salaryRequest, "salary request cannot be null");
        LOGGER.info("Create salary, salaryRequest = {}", salaryRequest);
        Salary salary = salaryService.createSalary(salaryRequest);
        return new AppResponse<>(of(salary), CREATE_SALARY_SUCCESS, SUCCESS);
    }
    @GetMapping("{salaryId}")
    public AppResponse<SalaryDto> getSalary(@PathVariable Long salaryId){
        requireNonNull(salaryId, "salary id cannot be null");
        LOGGER.info("Retrieving salary by id, salaryId = {}", salaryId);
        Salary salary = salaryService.findSalaryById(salaryId);
        return new AppResponse<>(of(salary), SALARY_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("payment-status/{paymentStatus}")
    public AppResponse<SalaryDto> getSalaryByPaymentStatus(@PathVariable String paymentStatus){
        requireNonNull(paymentStatus, "payment status cannot be null");
        LOGGER.info("Retrieving salary by payment status, paymentStatus = {}", paymentStatus);
        Salary salary = salaryService.findByPaymentStatus(paymentStatus);
        return new AppResponse<>(of(salary), SALARY_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<SalaryDto> getAllSalaries(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all salaries");
        Page<Salary> salaryPage = salaryService.findAllSalaries(pageable);
        return new PageImpl<>(of(salaryPage.getContent()), salaryPage.getPageable(),
                salaryPage.getTotalElements());
    }
    @PutMapping("{salaryId}")
    public AppResponse<SalaryDto> updateSalary(@PathVariable Long salaryId, @Valid @RequestBody
                                               SalaryUpdateRequest updateRequest){
        requireNonNull(salaryId, "salary id cannot be null");
        requireNonNull(updateRequest, "salary update request cannot be null");
        LOGGER.info("Update salary, salaryId = {}, updateRequest = {]", salaryId, updateRequest);
        Salary salary = salaryService.editSalary(salaryId, updateRequest);
        return new AppResponse<>(of(salary), SALARY_UPDATE_SUCCESS, SUCCESS);
    }
}
