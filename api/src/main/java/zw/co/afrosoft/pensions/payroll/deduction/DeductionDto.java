package zw.co.afrosoft.pensions.payroll.deduction;

import zw.co.afrosoft.pensions.payroll.enums.Category;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 12:37 PM
 **/

public class DeductionDto {
    private Long id;
    private String deductionName;
    private Double fixedAmount;
    private Double percentageAmount;
    private Category category;
    private Boolean isActive;
    private Boolean isFixed;

    public DeductionDto(Long id, String deductionName, Double fixedAmount, Double percentageAmount,
                        Category category, Boolean isActive, Boolean isFixed) {
        this.id = id;
        this.deductionName = deductionName;
        this.fixedAmount = fixedAmount;
        this.percentageAmount = percentageAmount;
        this.category = category;
        this.isActive = isActive;
        this.isFixed = isFixed;
    }
    public static DeductionDto of(Deduction deduction){
        if (isNull(deduction)){
            return null;
        }
        return new DeductionDto(deduction.getId(), deduction.getDeductionName(), deduction.getFixedAmount(),
                deduction.getPercentageAmount(), deduction.getCategory(), deduction.isActive(), deduction.isFixed());
    }
    public static List<DeductionDto> of(List<Deduction> deductions){
        if (isNull(deductions)){
            return null;
        }
        return deductions.stream().map(DeductionDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeductionName() {
        return deductionName;
    }

    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getPercentageAmount() {
        return percentageAmount;
    }

    public void setPercentageAmount(Double percentageAmount) {
        this.percentageAmount = percentageAmount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getFixed() {
        return isFixed;
    }

    public void setFixed(Boolean fixed) {
        isFixed = fixed;
    }
}
