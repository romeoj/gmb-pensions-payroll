package zw.co.afrosoft.pensions.payroll.branch;

import zw.co.afrosoft.pensions.payroll.bank.Bank;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/21/20
 * 9:30 PM
 **/
public class BranchDto {
    private Long id;
    private String branchName;
    private String branchCode;
    private Bank bank;
    private Boolean active;

    public BranchDto(Long id, String branchName, String branchCode, Bank bank, Boolean active) {
        this.id = id;
        this.branchName = branchName;
        this.branchCode = branchCode;
        this.bank = bank;
        this.active = active;
    }

    public static BranchDto of(Branch branch){
        if (isNull(branch)){
            throw new InvalidParameterException("Branch cannot be null");
        }
        return new BranchDto(branch.getId(), branch.getBranchName(), branch.getBranchCode(),
                branch.getBank(), branch.isActive());
    }
    public static List<BranchDto> of(List<Branch> branches){
        if (isNull(branches)){
            return null;
        }
        return branches.stream().map(BranchDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Boolean isActive() {
        return active;
    }

    public void setIsActive(Boolean active) {
        this.active = active;
    }
}
