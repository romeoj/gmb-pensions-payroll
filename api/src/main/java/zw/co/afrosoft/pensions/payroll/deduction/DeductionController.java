package zw.co.afrosoft.pensions.payroll.deduction;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.FAILED;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.deduction.DeductionDto.of;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/24/20
 * 12:38 PM
 **/

@RestController
@RequestMapping("v1/deduction")
public class DeductionController {
    private static final Logger LOGGER = getLogger(DeductionController.class);
    private final DeductionService deductionService;

    public DeductionController(DeductionService deductionService) {
        this.deductionService = deductionService;
    }
    @PostMapping
//    @PreAuthorize("hasAuthority(T(zw.co.afrosoft.pensions.payroll.enums.Authority)." + "CREATE_DEDUCTION)")
    public AppResponse<DeductionDto> createDeduction(@Valid @RequestBody DeductionCreateRequest createRequest){
        requireNonNull(createRequest, "deduction create request cannot be null");
        LOGGER.info("Create deduction, createRequest = {}", createRequest);
        Deduction deduction = deductionService.createDeduction(createRequest);
        if (isNull(deduction)){
            return new AppResponse<>(CREATE_DEDUCTION_FAILURE, FAILED);
        }
        return new AppResponse<>(of(deduction), CREATE_DEDUCTION_SUCCESS, SUCCESS);
    }
    @GetMapping("{deductionId}")
    public AppResponse<DeductionDto> getDeduction(@PathVariable Long deductionId){
        requireNonNull(deductionId, "deduction id cannot be null");
        LOGGER.info("Retrieving deduction by id, deductionId = {}", deductionId);
        Deduction deduction = deductionService.findDeductionById(deductionId);
        return new AppResponse<>(of(deduction), DEDUCTION_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("active/{isActive}")
    public AppResponse<DeductionDto> getDeductionByActiveStatus(@PathVariable Boolean isActive){
        requireNonNull(isActive, "active status cannot be null");
        LOGGER.info("Retrieving deduction by active status, isActive = {}", isActive);
        Deduction deduction = deductionService.findDeductionByIsActive(isActive);
        return new AppResponse<>(of(deduction), DEDUCTION_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("fixed/{isFixed}")
    public AppResponse<DeductionDto> getDeductionByFixedStatus(@PathVariable Boolean isFixed){
        requireNonNull(isFixed, "fixed status cannot be null");
        LOGGER.info("Retrieving deduction by fixed status, isFixed = {}", isFixed);
        Deduction deduction = deductionService.findDeductionByIsFixed(isFixed);
        return new AppResponse<>(of(deduction), DEDUCTION_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("name/{deductionName}")
    public AppResponse<DeductionDto> getDeductionByName(@PathVariable String deductionName){
        requireNonNull(deductionName, "deduction name cannot be null");
        LOGGER.info("Retrieving deduction by name, deductionName = {}", deductionName);
        Deduction deduction = deductionService.findDeductionByName(deductionName);
        return new AppResponse<>(of(deduction), DEDUCTION_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<DeductionDto> getAllDeduction(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all deductions");
        Page<Deduction> deductionPage = deductionService.findAllDeductions(pageable);
        if (deductionPage.isEmpty()){
            throw new RecordNotFoundException(DEDUCTION_SEARCH_FAILURE);
        }
        return new PageImpl<>(of(deductionPage.getContent()), deductionPage.getPageable(),
                deductionPage.getTotalElements());
    }
    @PutMapping("{deductionId}")
    public AppResponse<DeductionDto> updateDeduction(@PathVariable Long deductionId, @Valid
                                                     @RequestBody DeductionUpdateRequest updateRequest){
        requireNonNull(deductionId, "deduction id cannot be null");
        requireNonNull(updateRequest, "deduction update request cannot be null");
        LOGGER.info("Update deduction, deductionId = {}, updateRequest = {}", deductionId, updateRequest);
        Deduction deduction = deductionService.editDeduction(deductionId, updateRequest);
        return new AppResponse<>(of(deduction), UPDATE_DEDUCTION_SUCCESS, SUCCESS);
    }
}
