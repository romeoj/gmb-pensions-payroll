package zw.co.afrosoft.pensions.payroll.bank;

import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 3:22 PM
 **/
public class BankingDetailsDto {
    private Long id;
    private String accountNumber;
    private Bank bank;
    private Pensioner pensioner;

    public BankingDetailsDto(Long id, String accountNumber, Bank bank, Pensioner pensioner) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.bank = bank;
        this.pensioner = pensioner;
    }

    public static BankingDetailsDto of(BankingDetails bankingDetails){
        if (isNull(bankingDetails)){
            return null;
        }
        return  new BankingDetailsDto(bankingDetails.getId(),bankingDetails.getAccountNumber(),
                bankingDetails.getBank(), bankingDetails.getPensioner());
    }
    public static List<BankingDetailsDto> of(List<BankingDetails> bankingDetails){
        if (isNull(bankingDetails)){
            return null;
        }
        return bankingDetails.stream().map(BankingDetailsDto::of).collect(Collectors.toList());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Pensioner getPensioner() {
        return pensioner;
    }

    public void setPensioner(Pensioner pensioner) {
        this.pensioner = pensioner;
    }
}
