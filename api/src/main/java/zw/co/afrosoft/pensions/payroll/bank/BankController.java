package zw.co.afrosoft.pensions.payroll.bank;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.bank.BankDto.of;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.FAILED;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:14 PM
 **/

@RestController
@RequestMapping("v1/bank")
public class BankController {

    private static final Logger LOGGER = getLogger(BankController.class);
    private final BankService bankService;

    public BankController(BankService bankService) {
        this.bankService = bankService;
    }

    @PostMapping
    public AppResponse<BankDto> createBank(@Valid @RequestBody BankRequest bankRequest){
        requireNonNull(bankRequest, "bank request cannot be null");
        LOGGER.info("Create a bank request, bankRequest = {}", bankRequest);
        Bank bank = bankService.createBank(bankRequest);
        if (isNull(bank)){
            return new AppResponse<>(CREATE_BANK_FAILURE, FAILED);
        }
        return new AppResponse<>(of(bank), CREATE_BANK_SUCCESS, SUCCESS);
    }
    @GetMapping("/{bankId}")
    public AppResponse<BankDto> getBank(@PathVariable Long bankId){
        requireNonNull(bankId, "bank id cannot be null");
        LOGGER.info("Retrieving bank by id, bankId = {}", bankId);
        Bank bank = bankService.findBankById(bankId);
        return new AppResponse<>(of(bank),
                BANK_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("name/{bankName}")
    public AppResponse<BankDto> getBankByName(@PathVariable String bankName){
        requireNonNull(bankName, "bank name cannot be null");
        LOGGER.info("Retrieving bank by name, bankName = {} ", bankName);
        Bank bank = bankService.findBankByName(bankName);
        return new AppResponse<>(of(bank), BANK_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping("code/{bankCode}")
    public AppResponse<BankDto> getBankByCode(@PathVariable String bankCode){
        requireNonNull(bankCode, "bank code cannot be null");
        LOGGER.info("Retrieving bank by code, bankCode = {}", bankCode);
        Bank bank = bankService.findBankByCode(bankCode);
        return new AppResponse<>(of(bank), BANK_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<BankDto> getBanks(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving banks");
        Page<Bank> bankPage = bankService.findAllBanks(pageable);
        return new PageImpl<>(of(bankPage.getContent()),bankPage.getPageable(),
                bankPage.getTotalElements());
    }
    @PutMapping("{bankId}")
    public AppResponse<BankDto> updateBank(@PathVariable Long bankId,@RequestBody BankUpdateRequest updateRequest){
        requireNonNull(bankId, "bank id cannot be null");
        requireNonNull(updateRequest, "update request cannot be null");
        LOGGER.info("Updating bank, bankId = {}, updateRequest = {}", bankId, updateRequest);
        Bank bank = bankService.editBank(bankId, updateRequest);
        return new AppResponse<>(of(bank), BANK_UPDATE_SUCCESS, SUCCESS);
    }
}
