package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import zw.co.afrosoft.pensions.payroll.message.Message;
import zw.co.afrosoft.pensions.payroll.response.AppResponse;

import javax.validation.Valid;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static zw.co.afrosoft.pensions.payroll.beneficiary.BeneficiaryDto.of;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.FAILED;
import static zw.co.afrosoft.pensions.payroll.codes.ResponseCode.SUCCESS;
import static zw.co.afrosoft.pensions.payroll.message.Message.*;

/**
 * @Author Romeo J
 * 8/23/20
 * 8:35 PM
 **/

@RestController
@RequestMapping("v1/beneficiary")
public class BeneficiaryController {
    private static final Logger LOGGER = getLogger(BeneficiaryController.class);
    private final BeneficiaryService beneficiaryService;

    public BeneficiaryController(BeneficiaryService beneficiaryService) {
        this.beneficiaryService = beneficiaryService;
    }
    @PostMapping
    public AppResponse<BeneficiaryDto> createBeneficiary(@RequestBody BeneficiaryCreateRequest createRequest){
        requireNonNull(createRequest, "beneficiary create request cannot be null");
        LOGGER.info("Create beneficiary, createRequest = {}", createRequest);
        Beneficiary beneficiary = beneficiaryService.createBeneficiary(createRequest);
        if (isNull(beneficiary)){
            return new AppResponse<>(Message.CREATE_BENEFICIARY_FAILURE, FAILED);
        }
        return new AppResponse<>(of(beneficiary), CREATE_BENEFICIARY_SUCCESS, SUCCESS);
    }
    @GetMapping("{beneficiaryId}")
    public AppResponse<BeneficiaryDto> getBeneficiary(@PathVariable Long beneficiaryId){
        requireNonNull(beneficiaryId, "beneficiary id cannot be null");
        LOGGER.info("Retrieving beneficiary by id, beneficiaryId = {}", beneficiaryId);
        Beneficiary beneficiary = beneficiaryService.findBeneficiaryById(beneficiaryId);
        return new AppResponse<>(of(beneficiary), BENEFICIARY_SEARCH_SUCCESS, SUCCESS);
    }
    @GetMapping
    public Page<BeneficiaryDto> getAllBeneficiaries(@PageableDefault Pageable pageable){
        LOGGER.info("Retrieving all beneficiaries");
        Page<Beneficiary> beneficiaryPage = beneficiaryService.findAllBeneficiaries(pageable);
        return new PageImpl<>(of(beneficiaryPage.getContent()), beneficiaryPage.getPageable(),
                beneficiaryPage.getTotalElements());
    }
    @PutMapping("{beneficiaryId}")
    public AppResponse<BeneficiaryDto> updateBeneficiary(@PathVariable Long beneficiaryId,
                                                         @Valid @RequestBody BeneficiaryUpdateRequest updateRequest){
        requireNonNull(beneficiaryId, "beneficiary Id cannot be null");
        requireNonNull(updateRequest, "beneficiary update request cannot be null");
        LOGGER.info("Update beneficiary, beneficiaryId = {}, updateRequest = {}", beneficiaryId, updateRequest);
        Beneficiary beneficiary = beneficiaryService.editBeneficiary(beneficiaryId, updateRequest);
        return new AppResponse<>(of(beneficiary), BENEFICIARY_UPDATE_SUCCESS, SUCCESS);
    }
}
