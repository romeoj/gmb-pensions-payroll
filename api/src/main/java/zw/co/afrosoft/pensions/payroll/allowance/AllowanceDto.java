package zw.co.afrosoft.pensions.payroll.allowance;

import zw.co.afrosoft.pensions.payroll.enums.Category;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 10:18 AM
 **/

public class AllowanceDto {
    private Long id;
    private Double fixedAmount;
    private Double fixedPercentage;
    private String allowanceName;
    private Boolean taxable;
    private Category category;
    private Boolean active;
    private Boolean isFixed;
    private Double taxPercentage;

    public AllowanceDto(Long id, Double fixedAmount, Double fixedPercentage,
                        String allowanceName, Boolean taxable, Category category,
                        Boolean active, Boolean isFixed, Double taxPercentage) {
        this.id = id;
        this.fixedAmount = fixedAmount;
        this.fixedPercentage = fixedPercentage;
        this.allowanceName = allowanceName;
        this.taxable = taxable;
        this.category = category;
        this.active = active;
        this.isFixed = isFixed;
        this.taxPercentage = taxPercentage;
    }
    public static AllowanceDto of(Allowance allowance){
        if (isNull(allowance)){
            return null;
        }
        return new AllowanceDto(allowance.getId(), allowance.getFixedAmount(),
                allowance.getFixedPercentage(), allowance.getAllowanceName(),
                allowance.isTaxable(),allowance.getCategory(), allowance.isActive(),
                allowance.isFixed(), allowance.getTaxPercentage());
    }
    public static List<AllowanceDto> of(List<Allowance> allowances){
        if (isNull(allowances)){
            return null;
        }
        return allowances.stream().map(AllowanceDto::of).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getFixedPercentage() {
        return fixedPercentage;
    }

    public void setFixedPercentage(Double fixedPercentage) {
        this.fixedPercentage = fixedPercentage;
    }

    public String getAllowanceName() {
        return allowanceName;
    }

    public void setAllowanceName(String allowanceName) {
        this.allowanceName = allowanceName;
    }

    public Boolean getTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getFixed() {
        return isFixed;
    }

    public void setFixed(Boolean fixed) {
        isFixed = fixed;
    }

    public Double getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }
}
