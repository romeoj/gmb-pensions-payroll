package zw.co.afrosoft.pensions.payroll.enums;

/**
 * @Author Romeo J
 * 8/22/20
 * 8:54 AM
 **/

public enum Category {
    DEFAULT,
    GENERAL_STAFF,
    PENSION_FUND,
    EXECUTIVES,
    AGRIMED,
    INDIVIDUAL,
    ALL

}
