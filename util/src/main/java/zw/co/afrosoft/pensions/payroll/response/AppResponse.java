package zw.co.afrosoft.pensions.payroll.response;

import zw.co.afrosoft.pensions.payroll.codes.ResponseCode;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:57 PM
 **/
public class AppResponse<T> {
    private T data;
    private String narrative;
    private ResponseCode responseCode;
    private boolean success;

    public AppResponse() {
    }

    public AppResponse(T data, String narrative, ResponseCode responseCode) {
        this.data = data;
        this.narrative = narrative;
        this.responseCode = responseCode;
        success = ResponseCode.SUCCESS.equals(responseCode);
    }

    public AppResponse(String narrative, ResponseCode responseCode) {
        this.narrative = narrative;
        this.responseCode = responseCode;
        success = ResponseCode.SUCCESS.equals(responseCode);
    }

    public AppResponse(String narrative, ResponseCode responseCode, boolean success) {
        this.narrative = narrative;
        this.responseCode = responseCode;
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AppResponse{");
        sb.append("data=").append(data);
        sb.append(", narrative='").append(narrative).append('\'');
        sb.append(", responseCode=").append(responseCode);
        sb.append(", success=").append(success);
        sb.append('}');
        return sb.toString();
    }

}

