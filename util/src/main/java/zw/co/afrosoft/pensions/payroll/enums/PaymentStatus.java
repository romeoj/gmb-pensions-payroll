package zw.co.afrosoft.pensions.payroll.enums;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:38 PM
 **/

public enum PaymentStatus {
    PAID,
    PENDING,
    PROCESSED,
    DUE
}
