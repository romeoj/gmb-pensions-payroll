package zw.co.afrosoft.pensions.payroll.codes;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:58 PM
 **/
public enum ResponseCode {
    FAILED,
    FATAL,
    NOT_FOUND,
    SUCCESS
}
