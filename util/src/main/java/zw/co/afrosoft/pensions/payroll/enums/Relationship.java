package zw.co.afrosoft.pensions.payroll.enums;

/**
 * @Author Romeo J
 * 8/22/20
 * 8:19 PM
 **/

public enum Relationship {
    MOTHER,
    FATHER,
    BROTHER,
    SISTER,
    CHILD,
    SPOUSE,
    NEPHEW,
    COUSIN,
    AUNT,
    GRAND_PARENT
}
