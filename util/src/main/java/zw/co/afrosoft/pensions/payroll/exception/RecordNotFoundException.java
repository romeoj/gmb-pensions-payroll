package zw.co.afrosoft.pensions.payroll.exception;

/**
 * @Author Romeo J
 * 3/9/20
 * 5:08 PM
 **/

public class RecordNotFoundException extends RuntimeException {

    private static  String MESSAGE = "Record Not Found";

    public RecordNotFoundException(String s) {
        super(s);
    }

    public RecordNotFoundException() {
        this(MESSAGE);
    }

    @Override
    public String getMessage() {
        return MESSAGE;
    }
}
