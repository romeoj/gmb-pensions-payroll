package zw.co.afrosoft.pensions.payroll.enums;

/**
 * @Author Romeo J
 * 8/22/20
 * 4:45 PM
 **/
public enum PensionerStatus {
    DORMANT,
    ACTIVE,
    IN_ACTIVE,
    DECEASED
}
