package zw.co.afrosoft.pensions.payroll.message;

/**
 * @Author Romeo J
 * 3/9/20
 * 5:09 PM
 **/

public class Message {

    public static final String CREATE_BANK_SUCCESS = "Bank created successfully";
    public static final String CREATE_BANK_FAILURE = "Failed to create Bank";
    public static final String BANK_SEARCH_SUCCESS = "Bank found";
    public static final String BANK_UPDATE_SUCCESS = "Bank updated successfully";

    public static final String CREATE_BRANCH_SUCCESS = "Branch created successfully";
    public static final String BRANCH_SEARCH_SUCCESS = "Branch found";
    public static final String BRANCH_UPDATE_SUCCESS = "Branch updated successfully";

    public static final String CREATE_PENSIONER_SUCCESS = "Pensioner created successfully";
    public static final String UPDATE_PENSIONER_SUCCESS = "Pensioner updated successfully";
    public static final String PENSIONER_SEARCH_SUCCESS = "Pensioner found";

    public static final String CREATE_DEDUCTION_SUCCESS = "Deduction created successfully";
    public static final String CREATE_DEDUCTION_FAILURE = "Failed to create Deduction";
    public static final String UPDATE_DEDUCTION_SUCCESS = "Deduction updated successfully";
    public static final String DEDUCTION_SEARCH_SUCCESS = "Deduction found";
    public static final String DEDUCTION_SEARCH_FAILURE = "Deduction does not exist";

    public static final String CREATE_BANKING_DETAILS_SUCCESS = "BankingDetails created successfully";
    public static final String CREATE_BANKING_DETAILS_FAILURE = "Failed to create banking details";
    public static final String UPDATE_BANKING_DETAILS_SUCCESS = "BankingDetails updated successfully";
    public static final String BANKING_DETAILS_SEARCH_SUCCESS = "BankingDetails found";

    public static final String CREATE_BENEFICIARY_SUCCESS = "Beneficiary created successfully";
    public static final String CREATE_BENEFICIARY_FAILURE = "Failed to create Beneficial";
    public static final String BENEFICIARY_UPDATE_SUCCESS = "Beneficiary updated successfully";
    public static final String BENEFICIARY_SEARCH_SUCCESS = "Beneficiary found";

    public static final String CREATE_BENEFICIARY_DETAILS_SUCCESS = "Beneficiary details created successfully";
    public static final String CREATE_BENEFICIARY_DETAILS_FAILURE = "Failed to create Beneficiary details";
    public static final String BENEFICIARY_DETAILS_SEARCH_SUCCESS = "Beneficiary details found";
    public static final String BENEFICIARY_DETAILS_UPDATE_SUCCESS = "Beneficiary details updated successfully";

    public static final String CREATE_ALLOWANCE_SUCCESS = "Allowance created successfully";
    public static final String CREATE_ALLOWANCE_FAILURE = "Failed to create Allowance";
    public static final String ALLOWANCE_UPDATE_SUCCESS = "Allowance updated successfully";
    public static final String ALLOWANCE_SEARCH_SUCCESS = "Allowance found";

    public static final String CREATE_SALARY_SUCCESS = "Salary created successfully";
    public static final String SALARY_UPDATE_SUCCESS = "Salary updated successfully";
    public static final String SALARY_SEARCH_SUCCESS = "Salary found";

    public static final String CREATE_REMUNERATION_SUCCESS = "Remuneration created successfully";
    public static final String CREATE_REMUNERATION_FAILURE = "Failed to create Remuneration";
    public static final String REMUNERATION_SEARCH_SUCCESS = "Remuneration found";

    private Message() {
    }

}
