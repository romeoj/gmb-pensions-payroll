package zw.co.afrosoft.pensions.payroll.enums;

/**
 * @Author Romeo J
 * 8/25/20
 * 9:56 AM
 **/

public enum Authority {
    CREATE_ALLOWANCE(false),
    VIEW_ALLOWANCE(false),
    UPDATE_ALLOWANCE(false),
    CREATE_BANK(false),
    VIEW_BANK(false),
    UPDATE_BANK(false),
    CREATE_BANK_DETAILS(false),
    VIEW_BANK_DETAILS(false),
    UPDATE_BANK_DETAILS(false),
    CREATE_BENEFICIARY(false),
    VIEW_BENEFICIARY(false),
    UPDATE_BENEFICIARY(false),
    CREATE_BENEFICIARY_DETAILS(false),
    VIEW_BENEFICIARY_DETAILS(false),
    UPDATE_BENEFICIARY_DETAILS(false),
    CREATE_BRANCH(false),
    VIEW_BRANCH(false),
    UPDATE_BRANCH(false),
    CREATE_DEDUCTION(false),
    VIEW_DEDUCTION(false),
    UPDATE_DEDUCTION(false),
    PROCESS_PAYROLL(false),
    CREATE_PENSIONER(false),
    VIEW_PENSIONER(false),
    UPDATE_PENSIONER(false),
    CREATE_REMUNERATION(false),
    VIEW_REMUNERATION(false),
    UPDATE_REMUNERATION(false),
    CREATE_SALARY(false),
    VIEW_SALARY(false),
    UPDATE_SALARY(false),

    DEACTIVATE_USER_ACCOUNT(true),
    BLOCK_USER_ACCOUNT(true);

    private boolean admin;

    Authority(boolean admin) {
        this.admin = admin;
    }

}
