package zw.co.afrosoft.pensions.payroll;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * @Author Romeo J
 * 8/20/20
 * 4:19 PM
 **/

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("zw.co.afrosoft"))
                .paths(PathSelectors.any()).build()
                .apiInfo(apiInfo());
    }


    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Pensions: Pensions Payroll ",
                "Pensions Payroll Web Services ",
                "1.0.0",
                "Terms of service",
                new Contact("Afrosoft Engineering Team", "https://www.afrosoft.co.zw/#/", "developers@afrosoft.co.zw"),
                "License of API", "API license URL", Collections.emptyList());
    }

}
