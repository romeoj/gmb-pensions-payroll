package zw.co.afrosoft.pensions.payroll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.core.Ordered;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.security.crypto.factory.PasswordEncoderFactories;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
//import zw.co.afrosoft.commons.audittrail.configuration.EnableAuditTrail;
//import zw.co.afrosoft.commons.notifications.notificationdriver.EnableNotificationService;
//import zw.co.afrosoft.commons.useraccounts.configuration.EnableUserAccounts;
//import zw.co.afrosoft.commons.usercontext.EnableUserContextAwareness;

import java.util.Arrays;
import java.util.Collections;

/**
 * @Author Romeo J
 * 8/20/20
 * 3:35 PM
 **/

//@EnableUserAccounts
//@EnableNotificationService
//@EnableAuditTrail
@EnableSwagger2
//@EnableResourceServer
//@EnableAuthorizationServer
//@EnableUserContextAwareness
@EnableJpaRepositories("zw.co.afrosoft.pensions.payroll")
@EntityScan("zw.co.afrosoft.pensions.payroll")
@SpringBootApplication
public class PensionsPayrollApplication {
    public static void main(String[] args) {
        SpringApplication.run(PensionsPayrollApplication.class, args);
    }
//    @Bean
//    PasswordEncoder passwordEncoder() {
//        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
//    }
//
//    @Bean
//    public FilterRegistrationBean corsFilterBean() {
//        final CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Collections.singletonList("*"));
//        configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
//        configuration.setAllowCredentials(true);
//        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
//        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", configuration);
//        FilterRegistrationBean corsFilter = new FilterRegistrationBean(new CorsFilter(source));
//        corsFilter.setOrder(Ordered.HIGHEST_PRECEDENCE);
//        return corsFilter;
//    }

}
