package zw.co.afrosoft.pensions.payroll.bank;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

import java.security.InvalidParameterException;

import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/21/20
 * 1:47 PM
 **/

@Entity
@Table(name = "bank")
public class Bank {
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "bank_id")
    private Long id;
    @Column(name = "bank_name", nullable = false)
    private String bankName;
    @Column(name = "bank_code", nullable = false)
    private String bankCode;
    @Column(name = "active", nullable = false)
    private Boolean active;

    public Bank() { this.active = true;}

    public Bank(String bankName, String bankCode) {
        this();
        this.bankName = bankName;
        this.bankCode = bankCode;
    }

    public static Bank of(BankRequest bankRequest){
        if (isNull(bankRequest)){
            throw new InvalidParameterException("Bank request cannot be null");
        }
        return new Bank(bankRequest.getBankName(), bankRequest.getBankCode());
    }
    public Long getId() {
        return id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Bank{");
        sb.append("id=").append(id);
        sb.append(", bankName='").append(bankName).append('\'');
        sb.append(", bankCode='").append(bankCode).append('\'');
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
