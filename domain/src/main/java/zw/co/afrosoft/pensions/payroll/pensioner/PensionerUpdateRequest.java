package zw.co.afrosoft.pensions.payroll.pensioner;

import javax.validation.constraints.NotBlank;

/**
 * @Author Romeo J
 * 8/22/20
 * 1:53 PM
 **/

public class PensionerUpdateRequest {
    @NotBlank(message = "Pensioner number is required")
    private String pensionerNumber;
    @NotBlank(message = "Pensioner status is required")
    private String pensionerStatus;
    @NotBlank(message = "Category is required")
    private String category;

    public String getPensionerNumber() {
        return pensionerNumber;
    }

    public void setPensionerNumber(String pensionerNumber) {
        this.pensionerNumber = pensionerNumber;
    }

    public String getPensionerStatus() {
        return pensionerStatus;
    }

    public void setPensionerStatus(String pensionerStatus) {
        this.pensionerStatus = pensionerStatus;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PensionerUpdateRequest{");
        sb.append("pensionerNumber='").append(pensionerNumber).append('\'');
        sb.append(", pensionerStatus='").append(pensionerStatus).append('\'');
        sb.append(", category='").append(category).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
