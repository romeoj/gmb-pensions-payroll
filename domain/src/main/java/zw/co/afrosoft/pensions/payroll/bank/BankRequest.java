package zw.co.afrosoft.pensions.payroll.bank;

import javax.validation.constraints.NotBlank;

/**
 * @Author Romeo J
 * 8/21/20
 * 1:50 PM
 **/

public class BankRequest {
    @NotBlank(message = "Bank name is required")
    private String bankName;
    @NotBlank(message = "Bank code is required")
    private String bankCode;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankRequest{");
        sb.append("bankName='").append(bankName).append('\'');
        sb.append(", bankCode='").append(bankCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
