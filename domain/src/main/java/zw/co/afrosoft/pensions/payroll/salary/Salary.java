package zw.co.afrosoft.pensions.payroll.salary;

import zw.co.afrosoft.pensions.payroll.allowance.Allowance;
import zw.co.afrosoft.pensions.payroll.bank.BankingDetails;
import zw.co.afrosoft.pensions.payroll.deduction.Deduction;
import zw.co.afrosoft.pensions.payroll.enums.PaymentStatus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Enumerated;

import java.security.InvalidParameterException;

import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;
import static zw.co.afrosoft.pensions.payroll.enums.PaymentStatus.DUE;


/**
 * @Author Romeo J
 * 8/24/20
 * 1:59 PM
 **/

@Entity
@Table(name = "salary")
public class Salary {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @Column(name = "basic_amount", nullable = false)
    private Double basicAmount;
    @Column(name = "payable_amount")
    private Double payableAmount;
    @ManyToOne
    @JoinColumn(name = "deduction_id", referencedColumnName = "id")
    private Deduction deduction;
    @ManyToOne
    @JoinColumn(name = "allowance_id", referencedColumnName = "allowance_id")
    private Allowance allowance;
    @OneToOne
    private BankingDetails bankingDetails;
    @Column(name = "isAllowance")
    private Boolean allowanceStatus;
    @Enumerated
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    public Salary() {
        this.paymentStatus = DUE;
    }

    public Salary(Double basicAmount, Boolean allowanceStatus){
        this();
        this.basicAmount = basicAmount;
        this.allowanceStatus = allowanceStatus;
    }
    public static Salary of(SalaryRequest salaryRequest){
        if (isNull(salaryRequest)){
            throw new InvalidParameterException("Salary request cannot be null");
        }
        return new Salary(salaryRequest.getBasicAmount(), salaryRequest.getAllowance());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBasicAmount() {
        return basicAmount;
    }

    public void setBasicAmount(Double basicAmount) {
        this.basicAmount = basicAmount;
    }

    public Double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(Double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }

    public Allowance getAllowance() {
        return allowance;
    }

    public void setAllowance(Allowance allowance) {
        this.allowance = allowance;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    public Boolean getAllowanceStatus() {
        return allowanceStatus;
    }

    public void setAllowanceStatus(Boolean allowanceStatus) {
        this.allowanceStatus = allowanceStatus;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Salary{");
        sb.append("id=").append(id);
        sb.append(", basicAmount=").append(basicAmount);
        sb.append(", payableAmount=").append(payableAmount);
        sb.append(", deduction=").append(deduction);
        sb.append(", allowance=").append(allowance);
        sb.append(", bankingDetails=").append(bankingDetails);
        sb.append(", isAllowance=").append(allowanceStatus);
        sb.append(", paymentStatus=").append(paymentStatus);
        sb.append('}');
        return sb.toString();
    }
}
