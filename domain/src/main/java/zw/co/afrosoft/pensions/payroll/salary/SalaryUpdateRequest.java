package zw.co.afrosoft.pensions.payroll.salary;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 3:21 PM
 **/

public class SalaryUpdateRequest {
    @NotNull(message = "Basic amount is required")
    private Double basicAmount;
    @NotNull(message = "Allowance status is required")
    private Boolean isAllowance;
    @NotBlank(message = "Payment status is required")
    private String paymentStatus;

    public Double getBasicAmount() {
        return basicAmount;
    }

    public void setBasicAmount(Double basicAmount) {
        this.basicAmount = basicAmount;
    }

    public Boolean isAllowance() {
        return isAllowance;
    }

    public void setIsAllowance(Boolean isAllowance) {
        this.isAllowance = isAllowance;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
