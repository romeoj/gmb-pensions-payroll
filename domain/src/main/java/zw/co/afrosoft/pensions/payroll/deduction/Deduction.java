package zw.co.afrosoft.pensions.payroll.deduction;


import zw.co.afrosoft.pensions.payroll.enums.Category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/24/20
 * 10:33 AM
 **/

@Entity
@Table(name = "deduction")
public class Deduction {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @Column(name = "deductionName", unique = true)
    private String deductionName;
    @Column(name = "fixed_amount")
    private Double fixedAmount;
    @Column(name = "percentage_amount")
    private Double percentageAmount;
    @Column(name = "category")
    private Category category;
    @Column(name = "is_active", nullable = false)
    private Boolean isActive;
    @Column(name = "isFixed", nullable = false)
    private Boolean isFixed;
    @Column(name = "date_created")
    private LocalDateTime dateCreated;
    @Column(name = "date_updated")
    private LocalDateTime dateUpdated;

    public Deduction() {
        this.isActive = false;
        this.isFixed = false;
    }

    public Deduction(String name, Double fixedAmount, Double percentageAmount) {
        this();
        this.deductionName = name;
        this.fixedAmount = fixedAmount;
        this.percentageAmount = percentageAmount;
    }
    public static Deduction of(DeductionCreateRequest createRequest){
        if (isNull(createRequest)){
            throw new InvalidParameterException("Deduction create request cannot be null");
        }
        return new Deduction(createRequest.getDeductionName(), createRequest.getFixedAmount(),
                createRequest.getPercentageAmount());
    }

    @PrePersist
    public void init(){
        dateCreated = now();
    }
    @PreUpdate
    public void reload(){
        dateUpdated = now();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeductionName() {
        return deductionName;
    }

    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getPercentageAmount() {
        return percentageAmount;
    }

    public void setPercentageAmount(Double percentageAmount) {
        this.percentageAmount = percentageAmount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isFixed() {
        return isFixed;
    }

    public void setFixed(boolean isFixed) {
        this.isFixed = isFixed;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Deduction{");
        sb.append("id=").append(id);
        sb.append(", deductionName='").append(deductionName).append('\'');
        sb.append(", fixedAmount=").append(fixedAmount);
        sb.append(", percentageAmount=").append(percentageAmount);
        sb.append(", category=").append(category);
        sb.append(", isActive=").append(isActive);
        sb.append(", isFixed=").append(isFixed);
        sb.append(", dateCreated=").append(dateCreated);
        sb.append(", dateUpdated=").append(dateUpdated);
        sb.append('}');
        return sb.toString();
    }
}
