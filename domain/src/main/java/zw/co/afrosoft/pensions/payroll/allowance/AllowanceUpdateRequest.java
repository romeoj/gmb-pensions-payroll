package zw.co.afrosoft.pensions.payroll.allowance;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:31 AM
 **/

public class AllowanceUpdateRequest {
    @NotNull(message = "Fixed amount is required")
    private Double fixedAmount;
    @NotNull(message = "Fixed percentage is required")
    private Double fixedPercentage;
    @NotBlank(message = "Allowance name is requited")
    private String allowanceName;
    @NotNull(message = "Tax percentage is required")
    private Double taxPercentage;
    @NotBlank(message = "Category is required")
    private String category;
    @NotNull(message = "Active is required")
    private Boolean active;
    @NotNull(message = "Taxable is required")
    private Boolean taxable;
    @NotNull(message = "Fixed is required")
    private Boolean fixed;

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getFixedPercentage() {
        return fixedPercentage;
    }

    public void setFixedPercentage(Double fixedPercentage) {
        this.fixedPercentage = fixedPercentage;
    }

    public String getAllowanceName() {
        return allowanceName;
    }

    public void setAllowanceName(String allowanceName) {
        this.allowanceName = allowanceName;
    }

    public Double getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean isTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public Boolean isFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AllowanceUpdateRequest{");
        sb.append("fixedAmount=").append(fixedAmount);
        sb.append(", fixedPercentage=").append(fixedPercentage);
        sb.append(", allowanceName='").append(allowanceName).append('\'');
        sb.append(", taxPercentage=").append(taxPercentage);
        sb.append(", category='").append(category).append('\'');
        sb.append(", active=").append(active);
        sb.append(", taxable=").append(taxable);
        sb.append(", fixed=").append(fixed);
        sb.append('}');
        return sb.toString();
    }
}
