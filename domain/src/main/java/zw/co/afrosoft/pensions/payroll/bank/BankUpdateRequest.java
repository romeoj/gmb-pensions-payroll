package zw.co.afrosoft.pensions.payroll.bank;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/21/20
 * 4:49 PM
 **/

public class BankUpdateRequest {
    @NotBlank(message = "Name is required")
    private String bankName;
    @NotBlank(message = "Code is required")
    private String bankCode;
    @NotNull(message = "Active is required")
    private Boolean active;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankUpdateRequest{");
        sb.append("bankName='").append(bankName).append('\'');
        sb.append(", bankCode='").append(bankCode).append('\'');
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
