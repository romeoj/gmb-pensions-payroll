package zw.co.afrosoft.pensions.payroll.pensioner;

import zw.co.afrosoft.pensions.payroll.enums.Category;
import zw.co.afrosoft.pensions.payroll.enums.PensionerStatus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Enumerated;

import java.security.InvalidParameterException;

import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;
import static zw.co.afrosoft.pensions.payroll.enums.Category.DEFAULT;
import static zw.co.afrosoft.pensions.payroll.enums.PensionerStatus.ACTIVE;

/**
 * @Author Romeo J
 * 8/22/20
 * 11:47 AM
 **/

@Entity
@Table(name = "pensioner")
public class Pensioner {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @Column(name = "pensioner_number", nullable = false, unique = true)
    private String pensionerNumber;
    @Column(name = "pensioner_status")
    @Enumerated
    private PensionerStatus pensionerStatus;
    @Column(name = "category")
    @Enumerated
    private Category category;

    public Pensioner() {
        this.pensionerStatus = ACTIVE;
        this.category = DEFAULT;
    }

    public Pensioner(String pensionerNumber) {
        this();
        this.pensionerNumber = pensionerNumber;
    }
    public static Pensioner of (PensionerCreateRequest createRequest){
        if (isNull(createRequest)){
            throw new InvalidParameterException("Pensioner create request cannot be null");
        }
        return new Pensioner(createRequest.getPensionerNumber());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPensionerNumber() {
        return pensionerNumber;
    }

    public void setPensionerNumber(String pensionerNumber) {
        this.pensionerNumber = pensionerNumber;
    }

    public PensionerStatus getPensionerStatus() {
        return pensionerStatus;
    }

    public void setPensionerStatus(PensionerStatus pensionerStatus) {
        this.pensionerStatus = pensionerStatus;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pensioner{");
        sb.append("id=").append(id);
        sb.append(", pensionerNumber='").append(pensionerNumber).append('\'');
        sb.append(", pensionerStatus=").append(pensionerStatus);
        sb.append(", category=").append(category);
        sb.append('}');
        return sb.toString();
    }
}
