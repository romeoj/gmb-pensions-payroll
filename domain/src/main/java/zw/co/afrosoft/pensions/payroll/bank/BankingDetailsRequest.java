package zw.co.afrosoft.pensions.payroll.bank;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 12:03 PM
 **/

public class BankingDetailsRequest {
    @NotBlank(message = "Account number is required")
    private String accountNumber;
    @NotNull(message = "Bank id is required")
    private Long bankId;
    @NotBlank(message = "Pensioner number is required")
    private String pensionerNumber;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getPensionerNumber() {
        return pensionerNumber;
    }

    public void setPensionerNumber(String pensionerNumber) {
        this.pensionerNumber = pensionerNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankingDetailsRequest{");
        sb.append("accountNumber='").append(accountNumber).append('\'');
        sb.append(", bankId=").append(bankId);
        sb.append(", pensionerNumber='").append(pensionerNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
