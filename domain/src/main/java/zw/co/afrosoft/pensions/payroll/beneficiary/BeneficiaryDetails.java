package zw.co.afrosoft.pensions.payroll.beneficiary;

import zw.co.afrosoft.pensions.payroll.bank.BankingDetails;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import java.security.InvalidParameterException;
import java.time.LocalDate;

import static java.time.LocalDate.parse;
import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:42 PM
 **/

@Entity
@Table(name = "beneficiary_details")
public class BeneficiaryDetails {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @Column(name = "full_name", nullable = false)
    private String fullName;
    @Column(name = "date_of_birth", nullable = false)
    private LocalDate dateOfBirth;
    @Column(name = "id_number", nullable = false)
    private String nationalId;
    @OneToOne
    @JoinColumn(name = "banking_details_id", nullable = false)
    private BankingDetails bankingDetails;
    @ManyToOne
    @JoinColumn(name = "beneficiary_id", nullable = false)
    private Beneficiary beneficiary;

    public BeneficiaryDetails() {
    }

    public BeneficiaryDetails(String fullName, LocalDate dateOfBirth, String nationalId) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.nationalId = nationalId;
    }
    public static BeneficiaryDetails of(BeneficiaryDetailsRequest createRequest){
        if (isNull(createRequest)){
            throw new InvalidParameterException("Beneficiary details create request cannot be null");
        }
        return new BeneficiaryDetails(createRequest.getFullName(), parse(createRequest.getDateOfBirth()),
                createRequest.getNationalId());
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BeneficiaryDetails{");
        sb.append("id=").append(id);
        sb.append(", fullName='").append(fullName).append('\'');
        sb.append(", dateOfBirth=").append(dateOfBirth);
        sb.append(", nationalId='").append(nationalId).append('\'');
        sb.append(", bankingDetails=").append(bankingDetails);
        sb.append(", beneficiary=").append(beneficiary);
        sb.append('}');
        return sb.toString();
    }
}
