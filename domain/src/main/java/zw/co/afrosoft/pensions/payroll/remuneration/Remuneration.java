package zw.co.afrosoft.pensions.payroll.remuneration;

import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;
import zw.co.afrosoft.pensions.payroll.salary.Salary;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.ManyToOne;

import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static javax.persistence.GenerationType.AUTO;


/**
 * @Author Romeo J
 * 8/24/20
 * 6:17 PM
 **/

@Entity
@Table(name = "remuneration")
public class Remuneration {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @ManyToOne
    private Pensioner pensioner;
    @OneToOne
    private Salary salary;
    @Column(name = "date_created", nullable = false)
    private LocalDateTime dateCreated;

    private Remuneration() {
    }

    public static Remuneration of(){
        return new Remuneration();
    }
    @PrePersist
    public void init(){
        dateCreated = now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pensioner getPensioner() {
        return pensioner;
    }

    public void setPensioner(Pensioner pensioner) {
        this.pensioner = pensioner;
    }

    public Salary getSalary() {
        return salary;
    }

    public void setSalary(Salary salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Remuneration{");
        sb.append("id=").append(id);
        sb.append(", pensioner=").append(pensioner);
        sb.append(", salary=").append(salary);
        sb.append(", dateCreated=").append(dateCreated);
        sb.append('}');
        return sb.toString();
    }
}
