package zw.co.afrosoft.pensions.payroll.beneficiary;

import zw.co.afrosoft.pensions.payroll.enums.Relationship;
import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:40 PM
 **/

@Entity
@Table(name = "beneficiary")
public class Beneficiary {
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "beneficiary_id")
    private Long id;
    @Enumerated
    @Column(name = "relationship")
    private Relationship relationship;
    @ManyToOne
    @JoinColumn(name = "pensioner_number", nullable = false)
    Pensioner pensioner;
    @Column(name = "is_nominee")
    private Boolean isNominee;
    @Column(name = "is_beneficiary")
    private Boolean isBeneficiary;
    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    public Beneficiary() {
    }

    public Beneficiary(Boolean isNominee, Boolean isBeneficiary) {
        this.isNominee = isNominee;
        this.isBeneficiary = isBeneficiary;
    }

    public  static Beneficiary of(BeneficiaryCreateRequest createRequest){
        if (isNull(createRequest)){
            throw new InvalidParameterException("Beneficiary create request cannot be null");
        }
            return new Beneficiary(createRequest.isNominee(), createRequest.isBeneficiary());
    }
    @PrePersist
    public void init(){
        dateCreated = now();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public void setRelationship(Relationship relationship) {
        this.relationship = relationship;
    }

    public Pensioner getPensioner() {
        return pensioner;
    }

    public void setPensioner(Pensioner pensioner) {
        this.pensioner = pensioner;
    }

    public Boolean isNominee() {
        return isNominee;
    }

    public void setNominee(Boolean isNominee) {
        this.isNominee = isNominee;
    }

    public Boolean isBeneficiary() {
        return isBeneficiary;
    }

    public void setBeneficiary(Boolean isBeneficiary) {
        this.isBeneficiary = isBeneficiary;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Beneficiary{");
        sb.append("id=").append(id);
        sb.append(", relationship=").append(relationship);
        sb.append(", pensioner=").append(pensioner);
        sb.append(", isNominee=").append(isNominee);
        sb.append(", isBeneficiary=").append(isBeneficiary);
        sb.append(", dateCreated=").append(dateCreated);
        sb.append('}');
        return sb.toString();
    }
}
