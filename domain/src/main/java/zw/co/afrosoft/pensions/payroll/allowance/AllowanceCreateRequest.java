package zw.co.afrosoft.pensions.payroll.allowance;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:26 AM
 **/

public class AllowanceCreateRequest {
    @NotNull(message = "Fixed amount is required")
    private Double fixedAmount;
    @NotNull(message = "Fixed percentage is required")
    private Double fixedPercentage;
    @NotBlank(message = "Allowance name is required")
    private String allowanceName;
    @NotNull(message = "Tax percentage is required")
    private Double taxPercentage;
    @NotBlank(message = "Category is required")
    private String category;

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getFixedPercentage() {
        return fixedPercentage;
    }

    public void setFixedPercentage(Double fixedPercentage) {
        this.fixedPercentage = fixedPercentage;
    }

    public String getAllowanceName() {
        return allowanceName;
    }

    public void setAllowanceName(String allowanceName) {
        this.allowanceName = allowanceName;
    }

    public Double getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AllowanceCreateRequest{");
        sb.append("fixedAmount=").append(fixedAmount);
        sb.append(", fixedPercentage=").append(fixedPercentage);
        sb.append(", allowanceName='").append(allowanceName).append('\'');
        sb.append(", taxPercentage=").append(taxPercentage);
        sb.append(", category='").append(category).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
