package zw.co.afrosoft.pensions.payroll.branch;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/21/20
 * 8:09 PM
 **/

public class BranchRequest {
    @NotBlank(message = "Branch name is required")
    private String branchName;
    @NotBlank(message = "Branch code is required")
    private String branchCode;
    @NotNull(message = "Bank id is required")
    private Long bankId;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BranchRequest{");
        sb.append("branchName='").append(branchName).append('\'');
        sb.append(", branchCode='").append(branchCode).append('\'');
        sb.append(", bankId=").append(bankId);
        sb.append('}');
        return sb.toString();
    }
}
