package zw.co.afrosoft.pensions.payroll.deduction;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 11:35 AM
 **/

public class DeductionUpdateRequest {
    @NotBlank(message = "Deduction name is required")
    private String deductionName;
    @NotNull(message = "Fixed amount is required")
    private Double fixedAmount;
    @NotNull(message = "Percentage amount is required")
    private Double percentageAmount;
    @NotBlank(message = "Category is required")
    private String category;
    //@NotNull(message = "Active status cannot be null")
    private Boolean isActive;
    //@NotNull(message = "Fixed status cannot be null")
    private Boolean isFixed;

    public String getDeductionName() {
        return deductionName;
    }

    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getPercentageAmount() {
        return percentageAmount;
    }

    public void setPercentageAmount(Double percentageAmount) {
        this.percentageAmount = percentageAmount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isFixed() {
        return isFixed;
    }

    public void setIsFixed(Boolean isFixed) {
        this.isFixed = isFixed;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeductionUpdateRequest{");
        sb.append("deductionName='").append(deductionName).append('\'');
        sb.append(", fixedAmount=").append(fixedAmount);
        sb.append(", percentageAmount=").append(percentageAmount);
        sb.append(", category='").append(category).append('\'');
        sb.append(", isActive=").append(isActive);
        sb.append(", isFixed=").append(isFixed);
        sb.append('}');
        return sb.toString();
    }
}
