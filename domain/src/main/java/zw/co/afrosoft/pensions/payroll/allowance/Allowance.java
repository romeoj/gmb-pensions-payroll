package zw.co.afrosoft.pensions.payroll.allowance;

import zw.co.afrosoft.pensions.payroll.enums.Category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/22/20
 * 8:50 AM
 **/

@Entity
@Table(name = "allowance")
public class Allowance{
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "allowance_id")
    private Long id;
    @Column(name = "fixed_amount")
    private Double fixedAmount;
    @Column(name = "fixed_percentage")
    private Double fixedPercentage;
    @Column(name = "allowance_name")
    private String allowanceName;
    @Column(name = "isTaxable")
    private Boolean isTaxable;
    @Column(name = "category")
    @Enumerated
    private Category category;
    @Column(name = "date_created")
    private LocalDateTime dateCreated;
    @Column(name = "date_updated")
    private LocalDateTime dateUpdated;
    @Column(name = "isActive")
    private Boolean isActive;
    @Column(name = "isFixed")
    private Boolean isFixed;
    @Column(name = "taxPercentage")
    private Double taxPercentage;

    public Allowance() {
        this.isActive = false;
        this.isFixed = false;
        this.isTaxable = false;
    }

    public Allowance(Double fixedAmount, Double fixedPercentage, String allowanceName,
                      Double taxPercentage) {
        this();
        this.fixedAmount = fixedAmount;
        this.fixedPercentage = fixedPercentage;
        this.allowanceName = allowanceName;
        this.taxPercentage = taxPercentage;
    }

    public static Allowance of(AllowanceCreateRequest request){
        if (isNull(request)){
            throw new InvalidParameterException("Allowance create request cannot be null");
        }
        return new Allowance(request.getFixedAmount(), request.getFixedPercentage(),
                request.getAllowanceName(), request.getTaxPercentage());
    }
    @PrePersist
    private void init(){ dateCreated = now();}
    @PreUpdate
    private void reload(){ dateUpdated = now();}
    public Long getId() {
        return id;
    }

    public Double getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Double fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public Double getFixedPercentage() {
        return fixedPercentage;
    }

    public void setFixedPercentage(Double fixedPercentage) {
        this.fixedPercentage = fixedPercentage;
    }

    public String getAllowanceName() {
        return allowanceName;
    }

    public void setAllowanceName(String allowanceName) {
        this.allowanceName = allowanceName;
    }

    public Boolean isTaxable() {
        return isTaxable;
    }

    public void setIsTaxable(Boolean isTaxable) {
        this.isTaxable = isTaxable;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isFixed() {
        return isFixed;
    }

    public void setIsFixed(Boolean isFixed) {
        this.isFixed = isFixed;
    }

    public Double getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Allowance{");
        sb.append("id=").append(id);
        sb.append(", fixedAmount=").append(fixedAmount);
        sb.append(", fixedPercentage=").append(fixedPercentage);
        sb.append(", allowanceName='").append(allowanceName).append('\'');
        sb.append(", isTaxable=").append(isTaxable);
        sb.append(", category=").append(category);
        sb.append(", dateCreated=").append(dateCreated);
        sb.append(", dateUpdated=").append(dateUpdated);
        sb.append(", isActive=").append(isActive);
        sb.append(", isFixed=").append(isFixed);
        sb.append(", taxPercentage=").append(taxPercentage);
        sb.append('}');
        return sb.toString();
    }
}
