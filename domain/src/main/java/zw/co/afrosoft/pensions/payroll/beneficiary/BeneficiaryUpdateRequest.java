package zw.co.afrosoft.pensions.payroll.beneficiary;

import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 10:06 PM
 **/

public class BeneficiaryUpdateRequest {
    @NotNull(message = "Nominee status is required")
    private Boolean isNominee;
    @NotNull(message = "Beneficiary status is required")
    private Boolean isBeneficiary;

    public Boolean isNominee() {
        return isNominee;
    }

    public void setNominee(Boolean isNominee) {
        this.isNominee = isNominee;
    }

    public Boolean isBeneficiary() {
        return isBeneficiary;
    }

    public void setBeneficiary(Boolean isBeneficiary) {
        this.isBeneficiary = isBeneficiary;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BeneficiaryUpdateRequest{");
        sb.append(", isNominee=").append(isNominee);
        sb.append(", isBeneficiary=").append(isBeneficiary);
        sb.append('}');
        return sb.toString();
    }
}
