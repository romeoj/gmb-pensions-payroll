package zw.co.afrosoft.pensions.payroll.bank;

import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;


import java.security.InvalidParameterException;

import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/22/20
 * 11:46 AM
 **/

@Entity
@Table(name = "banking_details")
public class BankingDetails {
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column
    private Long id;
    @Column(name = "account_number", nullable = false)
    private String accountNumber;
    @OneToOne
    @JoinColumn(name = "bank_id", nullable = false)
    private Bank bank;
    @OneToOne
    @JoinColumn(name = "pensioner_number", nullable = false, unique = true)
    private Pensioner pensioner;

    public BankingDetails() {
    }

    public BankingDetails(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public static BankingDetails of(BankingDetailsRequest bankingRequest){
        if (isNull(bankingRequest)){
            throw new InvalidParameterException("Banking details request cannot be null");
        }
        return new BankingDetails(bankingRequest.getAccountNumber());
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Pensioner getPensioner() {
        return pensioner;
    }

    public void setPensioner(Pensioner pensioner) {
        this.pensioner = pensioner;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankingDetails{");
        sb.append("id=").append(id);
        sb.append(", accountNumber='").append(accountNumber).append('\'');
        sb.append(", bank=").append(bank);
        sb.append(", pensioner=").append(pensioner);
        sb.append('}');
        return sb.toString();
    }
}
