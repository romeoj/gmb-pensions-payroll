package zw.co.afrosoft.pensions.payroll.pensioner;

import javax.validation.constraints.NotBlank;

/**
 * @Author Romeo J
 * 8/22/20
 * 1:53 PM
 **/

public class PensionerCreateRequest {
    @NotBlank(message = "Pensioner number is required")
    private String pensionerNumber;

    public String getPensionerNumber() {
        return pensionerNumber;
    }

    public void setPensionerNumber(String pensionerNumber) {
        this.pensionerNumber = pensionerNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PensionerCreateRequest{");
        sb.append("pensionerNumber='").append(pensionerNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
