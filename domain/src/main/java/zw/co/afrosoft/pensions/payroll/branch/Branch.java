package zw.co.afrosoft.pensions.payroll.branch;

import zw.co.afrosoft.pensions.payroll.bank.Bank;

import javax.persistence.*;
import java.security.InvalidParameterException;

import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.AUTO;

/**
 * @Author Romeo J
 * 8/21/20
 * 1:45 PM
 **/

@Entity
@Table(name = "branch")
public class Branch {
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "branch_id")
    private Long id;
    @Column(name = "branch_name", nullable = false)
    private String branchName;
    @Column(name = "branch_code", nullable = false)
    private String branchCode;
    @ManyToOne()
    @JoinColumn(name = "bank_id", referencedColumnName = "bank_id")
    private Bank bank;
    @Column(name = "active", nullable = false)
    private Boolean active;

    public Branch() { this.active = true;}

    public Branch(String branchName, String branchCode) {
        this();
        this.branchName = branchName;
        this.branchCode = branchCode;
    }

    public static Branch of(BranchRequest branchRequest){
        if (isNull(branchRequest)){
            throw new InvalidParameterException("Branch request cannot be null");
        }
        return new Branch(branchRequest.getBranchName(), branchRequest.getBranchCode());

    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Boolean isActive() {
        return active;
    }

    public void setIsActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Branch{");
        sb.append("id=").append(id);
        sb.append(", branchName='").append(branchName).append('\'');
        sb.append(", branchCode='").append(branchCode).append('\'');
        sb.append(", bank=").append(bank);
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
