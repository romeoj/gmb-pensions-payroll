package zw.co.afrosoft.pensions.payroll.salary;

import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 2:11 PM
 **/

public class SalaryRequest {
    private Double basicAmount;
    private Long bankingDetailsId;
    private Long deductionId;
    private Long allowanceId;
    private Boolean isAllowance;

    public Double getBasicAmount() {
        return basicAmount;
    }

    public void setBasicAmount(Double basicAmount) {
        this.basicAmount = basicAmount;
    }

    public Long getBankingDetailsId() {
        return bankingDetailsId;
    }

    public void setBankingDetailsId(Long bankingDetailsId) {
        this.bankingDetailsId = bankingDetailsId;
    }

    public Long getDeductionId() {
        return deductionId;
    }

    public void setDeductionId(Long deductionId) {
        this.deductionId = deductionId;
    }

    public Long getAllowanceId() {
        return allowanceId;
    }

    public void setAllowanceId(Long allowanceId) {
        this.allowanceId = allowanceId;
    }

    public Boolean getAllowance() {
        return isAllowance;
    }

    public void setAllowance(Boolean allowance) {
        isAllowance = allowance;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SalaryRequest{");
        sb.append("basicAmount=").append(basicAmount);
        sb.append(", bankingDetailsId=").append(bankingDetailsId);
        sb.append(", deductionId=").append(deductionId);
        sb.append(", allowanceId=").append(allowanceId);
        sb.append(", isAllowance=").append(isAllowance);
        sb.append('}');
        return sb.toString();
    }
}
