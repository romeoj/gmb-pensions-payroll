package zw.co.afrosoft.pensions.payroll.beneficiary;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 10:00 PM
 **/

public class BeneficiaryCreateRequest {
    @NotBlank(message = "Relationship is required")
    private String relationship;
    @NotNull(message = "Pensioner id is required")
    private String  pensionerNumber;
    @NotNull(message = "Nominee status is required")
    private Boolean isNominee;
    @NotNull(message = "Beneficiary status is required")
    private Boolean isBeneficiary;


    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Boolean isNominee() {
        return isNominee;
    }

    public void setNominee(Boolean isNominee) {
        this.isNominee = isNominee;
    }

    public Boolean isBeneficiary() {
        return isBeneficiary;
    }

    public void setBeneficiary(Boolean isBeneficiary) {
       this.isBeneficiary = isBeneficiary;
    }

    public String getPensionerNumber() {
        return pensionerNumber;
    }

    public void setPensionerId(String pensionerNumber) {
        this.pensionerNumber = pensionerNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BeneficiaryCreateRequest{");
        sb.append("relationship='").append(relationship).append('\'');
        sb.append(", pensionerId=").append(pensionerNumber);
        sb.append(", isNominee=").append(isNominee);
        sb.append(", isBeneficiary=").append(isBeneficiary);
        sb.append('}');
        return sb.toString();
    }
}
