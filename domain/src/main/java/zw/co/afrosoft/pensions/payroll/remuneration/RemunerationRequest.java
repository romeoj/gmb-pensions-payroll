package zw.co.afrosoft.pensions.payroll.remuneration;

import javax.validation.constraints.NotNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 8:22 PM
 **/

public class RemunerationRequest {
    @NotNull(message = "Pensioner id is required")
    private Long pensionerId;
    @NotNull(message = "Salary id is required")
    private Long salaryId;

    public Long getPensionerId() {
        return pensionerId;
    }

    public void setPensionerId(Long pensionerId) {
        this.pensionerId = pensionerId;
    }

    public Long getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(Long salaryId) {
        this.salaryId = salaryId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RemunerationRequest{");
        sb.append("pensionerId=").append(pensionerId);
        sb.append(", salaryId=").append(salaryId);
        sb.append('}');
        return sb.toString();
    }
}
