package zw.co.afrosoft.pensions.payroll.beneficiary;

/**
 * @Author Romeo J
 * 8/22/20
 * 10:00 PM
 **/

public class BeneficiaryDetailsRequest {
    private String fullName;
    private String dateOfBirth;
    private String nationalId;
    private Long bankingDetailsId;
    private Long beneficiaryId;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public Long getBankingDetailsId() {
        return bankingDetailsId;
    }

    public void setBankingDetailsId(Long bankingDetailsId) {
        this.bankingDetailsId = bankingDetailsId;
    }

    public Long getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(Long beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BeneficiaryDetailsRequest{");
        sb.append("fullName='").append(fullName).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", nationalId='").append(nationalId).append('\'');
        sb.append(", bankingDetailsId=").append(bankingDetailsId);
        sb.append(", beneficiaryId=").append(beneficiaryId);
        sb.append('}');
        return sb.toString();
    }
}
