package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/23/20
 * 10:41 AM
 **/

public interface BeneficiaryService {
    Beneficiary createBeneficiary(BeneficiaryCreateRequest request);

    Beneficiary findBeneficiaryById(Long id);

    Page<Beneficiary> findAllBeneficiaries(Pageable pageable);

    Beneficiary editBeneficiary(Long id, BeneficiaryUpdateRequest request);

}
