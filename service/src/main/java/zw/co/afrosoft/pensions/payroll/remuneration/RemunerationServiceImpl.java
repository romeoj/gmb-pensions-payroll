package zw.co.afrosoft.pensions.payroll.remuneration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;
import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;
import zw.co.afrosoft.pensions.payroll.pensioner.PensionerService;
import zw.co.afrosoft.pensions.payroll.salary.Salary;
import zw.co.afrosoft.pensions.payroll.salary.SalaryService;

import static java.util.Objects.requireNonNull;

/**
 * @Author Romeo J
 * 8/24/20
 * 8:32 PM
 **/

@Service
public class RemunerationServiceImpl implements RemunerationService{
    private static final Logger LOGGER = LoggerFactory.getLogger(RemunerationServiceImpl.class);
    private final RemunerationRepository remunerationRepository;
    private final PensionerService pensionerService;
    private final SalaryService salaryService;

    public RemunerationServiceImpl(RemunerationRepository remunerationRepository, PensionerService pensionerService,
                                   SalaryService salaryService) {
        this.remunerationRepository = remunerationRepository;
        this.pensionerService = pensionerService;
        this.salaryService = salaryService;
    }

    @Override
    public Remuneration createRemuneration(RemunerationRequest request) {
        requireNonNull(request, "remuneration request cannot be null");
        LOGGER.info("Create remuneration, request = {}", request);
        Remuneration remuneration = Remuneration.of();
        Pensioner pensioner = pensionerService.findPensionerById(request.getPensionerId());
        requireNonNull(pensioner, "pensioner cannot be null");
        Salary salary = salaryService.findSalaryById(request.getSalaryId());
        requireNonNull(salary, "salary cannot be null");
        remuneration.setPensioner(pensioner);
        remuneration.setSalary(salary);
        return remunerationRepository.save(remuneration);
    }

    @Override
    public Remuneration findRemunerationById(Long remunerationId) {
        requireNonNull(remunerationId, "remuneration id cannot be null");
        LOGGER.info("Retrieving remuneration by id, remunerationId = {}", remunerationId);
        return remunerationRepository.findById(remunerationId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Page<Remuneration> findAllRemunerations(Pageable pageable) {
        return remunerationRepository.findAll(pageable);
    }
}
