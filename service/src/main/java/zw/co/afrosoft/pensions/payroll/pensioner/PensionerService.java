package zw.co.afrosoft.pensions.payroll.pensioner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/22/20
 * 1:50 PM
 **/
public interface PensionerService {
    Pensioner createPensioner(PensionerCreateRequest request);

    Pensioner findPensionerById(Long pensionerId);

    Pensioner findPensionerByPensionerNumber(String pensionerNumber);

    Pensioner findPensionerByPensionerStatus(String pensionerStatus);

    Pensioner editPensioner(Long pensionerId, PensionerUpdateRequest request);

    Page<Pensioner> findAllPensioner(Pageable pageable);

}
