package zw.co.afrosoft.pensions.payroll.allowance;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:40 AM
 **/

public interface AllowanceService {
    Allowance createAllowance(AllowanceCreateRequest request);

    Allowance findAllowanceById(Long allowanceId);

    Allowance findByIsActive(Boolean isActive);

    Allowance findByIsTaxable(Boolean isTaxable);

    Allowance findByAllowanceName(String allowanceName);

    Page<Allowance> findAllAllowances(Pageable pageable);

    Allowance editAllowance(Long allowanceId, AllowanceUpdateRequest updateRequest);

}
