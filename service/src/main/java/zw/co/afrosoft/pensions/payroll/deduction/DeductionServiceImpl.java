package zw.co.afrosoft.pensions.payroll.deduction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.enums.Category.valueOf;

/**
 * @Author Romeo J
 * 8/24/20
 * 11:37 AM
 **/

@Service
public class DeductionServiceImpl implements DeductionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeductionServiceImpl.class);
    private final DeductionRepository deductionRepository;

    public DeductionServiceImpl(DeductionRepository deductionRepository) {
        this.deductionRepository = deductionRepository;
    }

    @Override
    public Deduction createDeduction(DeductionCreateRequest createRequest) {
        requireNonNull(createRequest, "deduction create request cannot be null");
        LOGGER.info("Create deduction, createRequest = {}", createRequest);
        Deduction deduction = Deduction.of(createRequest);
        deduction.setCategory(valueOf(createRequest.getCategory()));
        return deductionRepository.save(deduction);
    }

    @Override
    public Deduction findDeductionById(Long deductionId) {
        requireNonNull(deductionId, "deduction id cannot be null");
        LOGGER.info("Retrieving deduction by id, deductionId = {}", deductionId);
        return deductionRepository.findById(deductionId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Deduction findDeductionByName(String deductionName) {
        requireNonNull(deductionName, "deduction name cannot be null");
        LOGGER.info("Retrieving deduction by name, deductionName = {}", deductionName);
        return deductionRepository.findByDeductionName(deductionName).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Deduction findDeductionByIsActive(Boolean isActive) {
        requireNonNull(isActive, "isActive status cannot be null");
        LOGGER.info("Retrieving deduction by active status, deductionId = {}", isActive);
        return deductionRepository.findByIsActive(isActive).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Deduction findDeductionByIsFixed(Boolean isFixed) {
        requireNonNull(isFixed, "isFixed status cannot be null");
        LOGGER.info("Retrieving deduction by fixed status, deductionId = {}", isFixed);
        return deductionRepository.findByIsFixed(isFixed).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Page<Deduction> findAllDeductions(Pageable pageable) {
        LOGGER.info("Retrieving all deductions");
        return deductionRepository.findAll(pageable);
    }

    @Override
    public Deduction editDeduction(Long deductionId, DeductionUpdateRequest updateRequest) {
        requireNonNull(deductionId, "deduction id cannot be null");
        requireNonNull(updateRequest, "deduction update request cannot be null");
        LOGGER.info("Update deduction, deductionId = {}, updateRequest = {}", deductionId, updateRequest);
        Deduction deduction = findDeductionById(deductionId);
        deduction.setDeductionName(updateRequest.getDeductionName());
        deduction.setFixedAmount(updateRequest.getFixedAmount());
        deduction.setCategory(valueOf(updateRequest.getCategory()));
        deduction.setActive(updateRequest.isActive());
        deduction.setFixed(updateRequest.isFixed());
        return deductionRepository.save(deduction);
    }
}
