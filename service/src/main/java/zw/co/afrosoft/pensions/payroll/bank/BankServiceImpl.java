package zw.co.afrosoft.pensions.payroll.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.bank.Bank.of;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:08 PM
 **/
@Service
public class BankServiceImpl implements BankService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BankServiceImpl.class);
    private final BankRepository bankRepository;

    public BankServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public Bank createBank(BankRequest bankRequest) {
        requireNonNull(bankRequest, "bank request cannot be null");
        LOGGER.info("Create bank request, bankRequest = {}", bankRequest);
        Bank bank = of(bankRequest);
        return bankRepository.save(bank);
    }

    @Override
    public Bank findBankById(Long bankId) {
        requireNonNull(bankId, "bank id cannot be null");
        LOGGER.info("Retrieving bank by id, bankId = {}", bankId);
        return bankRepository.findById(bankId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Bank findBankByCode(String bankCode) {
        requireNonNull(bankCode, "bank code cannot be null");
        LOGGER.info("Retrieving bank by code, bankId = {}", bankCode);
        return bankRepository.findBankByBankCode(bankCode).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Bank findBankByName(String bankName) {
        requireNonNull(bankName, "bank name cannot be null");
        LOGGER.info("Retrieving bank by name, bankId = {}", bankName);
        return bankRepository.findBankByBankName(bankName).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Page<Bank> findAllBanks(Pageable pageable) {
        LOGGER.info("Retrieving all Banks");
        return bankRepository.findAll(pageable);
    }

    @Override
    public Bank editBank(Long bankId, BankUpdateRequest bankUpdateRequest) {
        requireNonNull(bankId, "bank id cannot be null");
        requireNonNull(bankUpdateRequest, "bank update request cannot be null");
        LOGGER.info("Updating bank, bankId = {}, bankUpdateRequest = {}", bankId, bankUpdateRequest);
        Bank bank = findBankById(bankId);
        bank.setBankCode(bankUpdateRequest.getBankCode());
        bank.setBankName(bankUpdateRequest.getBankName());
        bank.setActive(bankUpdateRequest.isActive());
        return bankRepository.save(bank);
    }
}
