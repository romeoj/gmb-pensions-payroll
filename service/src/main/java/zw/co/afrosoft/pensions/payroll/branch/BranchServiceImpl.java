package zw.co.afrosoft.pensions.payroll.branch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.bank.Bank;
import zw.co.afrosoft.pensions.payroll.bank.BankService;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.branch.Branch.of;

/**
 * @Author Romeo J
 * 8/21/20
 * 8:32 PM
 **/

@Service
public class BranchServiceImpl implements BranchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BranchServiceImpl.class);
    private final BranchRepository branchRepository;
    public final BankService bankService;

    public BranchServiceImpl(BranchRepository branchRepository, BankService bankService) {
        this.branchRepository = branchRepository;
        this.bankService = bankService;
    }

    @Override
    public Branch createBranch(BranchRequest branchRequest) {
        requireNonNull(branchRequest, "branch request cannot be null");
        LOGGER.info("Creating branch request, branchRequest = {}", branchRequest);
        Branch branch = of(branchRequest);
        Bank bank = bankService.findBankById(branchRequest.getBankId());
        requireNonNull(bank, "bank cannot be null");
        branch.setBank(bank);
        return branchRepository.save(branch);
    }

    @Override
    public Branch findBranchById(Long branchId) {
        requireNonNull(branchId, "branch id cannot be null");
        LOGGER.info("Retrieving branch by kid, branchId = {}", branchId);
        return branchRepository.findById(branchId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Branch findBranchByBranchName(String branchName) {
        requireNonNull(branchName, "branch name cannot be null");
        LOGGER.info("Retrieving branch by name, branchName = {}", branchName);
        return branchRepository.findByBranchName(branchName).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Branch findBranchByBranchCode(String branchCode) {
        requireNonNull(branchCode, "branch code cannot be null");
        LOGGER.info("Retrieving branch by code, branchCode = {}", branchCode);
        return branchRepository.findByBranchCode(branchCode).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Page<Branch> findAllBranches(Pageable pageable) {
        LOGGER.info("Retrieving all branches");
        return branchRepository.findAll(pageable);
    }

    @Override
    public Branch editBranch(Long branchId, BranchUpdateRequest updateRequest) {
        requireNonNull(branchId, "branch id cannot be null");
        requireNonNull(updateRequest, "branch update request cannot be null");
        LOGGER.info("Updating branch, branchId = {}, updateRequest = {}", branchId, updateRequest);
        Branch branch = findBranchById(branchId);
        Bank bank = bankService.findBankById(updateRequest.getBankId());
        requireNonNull(bank, "bank cannot be null");
        branch.setBranchName(updateRequest.getBranchName());
        branch.setBranchCode(updateRequest.getBranchCode());
        branch.setBank(bank);
        branch.setIsActive(updateRequest.isActive());
        return branchRepository.save(branch);
    }
}
