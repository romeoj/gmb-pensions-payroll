package zw.co.afrosoft.pensions.payroll.report;

import net.sf.jasperreports.engine.JRException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

/**
 * @Author Romeo J
 * 8/25/20
 * 12:39 PM
 **/

public interface ReportService {
    String generateReport(String reportName, Map<String, Object> params, HttpServletResponse response) throws
            IOException, JRException, SQLException;

}
