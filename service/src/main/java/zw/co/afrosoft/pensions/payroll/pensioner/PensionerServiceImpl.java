package zw.co.afrosoft.pensions.payroll.pensioner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.enums.Category;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.enums.PensionerStatus.valueOf;

import static zw.co.afrosoft.pensions.payroll.pensioner.Pensioner.of;

/**
 * @Author Romeo J
 * 8/22/20
 * 1:54 PM
 **/
@Service
public class PensionerServiceImpl implements PensionerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PensionerServiceImpl.class);
    private final PensionerRepository pensionerRepository;

    public PensionerServiceImpl(PensionerRepository pensionerRepository) {
        this.pensionerRepository = pensionerRepository;
    }

    @Override
    public Pensioner createPensioner(PensionerCreateRequest request) {
        requireNonNull(request, "pensioner create request cannot be null");
        LOGGER.info("Creating pensioner, request = {}", request);
        Pensioner pensioner = of(request);
        return pensionerRepository.save(pensioner);
    }

    @Override
    public Pensioner findPensionerById(Long pensionerId) {
        requireNonNull(pensionerId, "pensioner id cannot be null");
        LOGGER.info("Retrieving pensioner by id, pensionerId = {}", pensionerId);
        return pensionerRepository.findById(pensionerId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Pensioner findPensionerByPensionerNumber(String pensionerNumber) {
        requireNonNull(pensionerNumber, "pensioner number cannot be null");
        LOGGER.info("Retrieving pensioner by pensioner number, pensionerNumber = {}", pensionerNumber);
        return pensionerRepository.findByPensionerNumber(pensionerNumber).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Pensioner findPensionerByPensionerStatus(String pensionerStatus) {
        requireNonNull(pensionerStatus, "pensioner status cannot be null");
        LOGGER.info("Retrieving pensioner by pensioner status, pensionerStatus = {}", pensionerStatus);
        return pensionerRepository.findByPensionerStatus(valueOf(pensionerStatus)).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Pensioner editPensioner(Long pensionerId, PensionerUpdateRequest updateRequest) {
        requireNonNull(pensionerId, "pensioner id cannot be null");
        requireNonNull(updateRequest, "pensioner update updateRequest cannot be null");
        LOGGER.info("Updating pensioner , pensionerId = {}", pensionerId);
        Pensioner pensioner = findPensionerById(pensionerId);
        pensioner.setPensionerNumber(updateRequest.getPensionerNumber());
        pensioner.setCategory(Category.valueOf(updateRequest.getCategory()));
        pensioner.setPensionerStatus(valueOf(updateRequest.getPensionerStatus()));
        return pensionerRepository.save(pensioner);
    }

    @Override
    public Page<Pensioner> findAllPensioner(Pageable pageable) {
        return pensionerRepository.findAll(pageable);
    }
}
