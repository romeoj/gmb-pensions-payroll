package zw.co.afrosoft.pensions.payroll.bank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/22/20
 * 12:18 PM
 **/

public interface BankingDetailsService {
    BankingDetails createBankingDetails(BankingDetailsRequest request);

    BankingDetails findBankingDetailsById(Long bankingDetailsId);

    BankingDetails findBankingDetailsByAccountNumber(String accountNumber);

    BankingDetails editBankingDetails(Long bankingDetailsId, BankingDetailsUpdateRequest updateRequest);

    Page<BankingDetails> findAllBankingDetails(Pageable pageable);

}
