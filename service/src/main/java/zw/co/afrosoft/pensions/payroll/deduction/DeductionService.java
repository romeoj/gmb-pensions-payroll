package zw.co.afrosoft.pensions.payroll.deduction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/24/20
 * 11:30 AM
 **/

public interface DeductionService {
    Deduction createDeduction(DeductionCreateRequest request);

    Deduction findDeductionById(Long deductionId);

    Deduction findDeductionByName(String deductionName);

    Deduction findDeductionByIsActive(Boolean isActive);

    Deduction findDeductionByIsFixed(Boolean isFixed);

    Page<Deduction> findAllDeductions(Pageable pageable);

    Deduction editDeduction(Long deductionId, DeductionUpdateRequest request);

}
