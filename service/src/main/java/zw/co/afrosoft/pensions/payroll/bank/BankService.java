package zw.co.afrosoft.pensions.payroll.bank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

/**
 * @Author Romeo J
 * 8/21/20
 * 2:05 PM
 **/
public interface BankService {
    Bank createBank(BankRequest bankRequest);

    Bank findBankById(Long id);

    Bank findBankByCode(String bankCode);

    Bank findBankByName(String bankName);

    Page<Bank> findAllBanks(Pageable pageable);

    Bank editBank(Long id, BankUpdateRequest bankUpdateRequest);

}
