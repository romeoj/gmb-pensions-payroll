package zw.co.afrosoft.pensions.payroll.branch;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/21/20
 * 8:26 PM
 **/

public interface BranchService {

    Branch createBranch(BranchRequest branchRequest);

    Branch findBranchById(Long branchId);

    Branch findBranchByBranchName(String branchName);

    Branch findBranchByBranchCode(String branchCode);

    Page<Branch> findAllBranches(Pageable pageable);

    Branch editBranch(Long branchId, BranchUpdateRequest updateRequest);
}
