package zw.co.afrosoft.pensions.payroll.salary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import zw.co.afrosoft.pensions.payroll.enums.PaymentStatus;

/**
 * @Author Romeo J
 * 8/24/20
 * 2:55 PM
 **/

public interface SalaryService {
    Salary createSalary(SalaryRequest salaryRequest);

    Salary findSalaryById(Long salaryId);

    Salary findByPaymentStatus(String paymentStatus);

    Page<Salary> findAllSalaries(Pageable pageable);

    Salary editSalary(Long salaryId, SalaryUpdateRequest updateRequest);

}
