package zw.co.afrosoft.pensions.payroll.salary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.allowance.Allowance;
import zw.co.afrosoft.pensions.payroll.allowance.AllowanceService;
import zw.co.afrosoft.pensions.payroll.bank.BankingDetailsService;
import zw.co.afrosoft.pensions.payroll.deduction.Deduction;
import zw.co.afrosoft.pensions.payroll.deduction.DeductionService;
import zw.co.afrosoft.pensions.payroll.enums.PaymentStatus;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.enums.PaymentStatus.valueOf;
import static zw.co.afrosoft.pensions.payroll.salary.Salary.of;

/**
 * @Author Romeo J
 * 8/24/20
 * 2:56 PM
 **/

@Service
public class SalaryServiceImpl implements SalaryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalaryServiceImpl.class);
    private final SalaryRepository salaryRepository;
    private final BankingDetailsService bankingDetailsService;
    private final DeductionService deductionService;
    private final AllowanceService allowanceService;

    public SalaryServiceImpl(SalaryRepository salaryRepository, BankingDetailsService bankingDetailsService,
                             DeductionService deductionService, AllowanceService allowanceService) {
        this.salaryRepository = salaryRepository;
        this.bankingDetailsService = bankingDetailsService;
        this.deductionService = deductionService;
        this.allowanceService = allowanceService;
    }

    @Override
    public Salary createSalary(SalaryRequest salaryRequest) {
        requireNonNull(salaryRequest, "salary request cannot be null");
        LOGGER.info("Create Salary, salaryRequest = {}", salaryRequest);
        Salary salary = of(salaryRequest);
        Allowance allowance = allowanceService.findAllowanceById(salaryRequest.getAllowanceId());
        Deduction deduction = deductionService.findDeductionById(salaryRequest.getDeductionId());
        requireNonNull(allowance, "allowance cannot be null");
        requireNonNull(deduction, "deduction cannot be null");
        salary.setAllowance(allowance);
        salary.setDeduction(deduction);
        salary.setBankingDetails(bankingDetailsService.findBankingDetailsById(salaryRequest.getBankingDetailsId()));
        processAllowanceAndDeduction(salary, allowance, deduction);
        return salaryRepository.save(salary);
    }
    private void processAllowanceAndDeduction(Salary salary, Allowance allowance, Deduction deduction) {
        Double amount;
        Double taxedAllowance;
        Double deductedAmount;
        if (salary.getAllowanceStatus()) {
            if (allowance.isFixed()) {
                if (allowance.isTaxable()) {
                    if (deduction.isFixed()) {
                        taxedAllowance = (allowance.getTaxPercentage() / 100) * allowance.getFixedAmount();
                        amount = allowance.getFixedAmount() - taxedAllowance;
                        salary.setPayableAmount((salary.getBasicAmount() + amount) - deduction.getFixedAmount());
                    }
                } else {
                    deductedAmount = (deduction.getPercentageAmount() / 100) * salary.getBasicAmount();
                    salary.setPayableAmount((salary.getBasicAmount() + allowance.getFixedAmount()) - deductedAmount);
                }
            } else if (!allowance.isFixed()) {
                if (allowance.isTaxable()) {
                    if (!deduction.isFixed()) {
                        deductedAmount = (deduction.getPercentageAmount() / 100) * salary.getBasicAmount();
                        taxedAllowance = (allowance.getTaxPercentage() / 100) * salary.getBasicAmount();
                        amount = (allowance.getFixedPercentage() / 100) * salary.getBasicAmount();
                        salary.setPayableAmount((salary.getBasicAmount() + (amount - taxedAllowance)) - deductedAmount);
                    }
                } else {
                    taxedAllowance = (allowance.getTaxPercentage() / 100) * allowance.getFixedAmount();
                    amount = (allowance.getFixedPercentage() / 100) * salary.getBasicAmount();
                    salary.setPayableAmount((salary.getBasicAmount() + amount) - taxedAllowance);
                }
            }
        }

    }

    @Override
    public Salary findSalaryById(Long salaryId) {
        requireNonNull(salaryId, "salary id cannot be null");
        LOGGER.info("Retrieving salary by id, salaryId = {}", salaryId);
        return salaryRepository.findById(salaryId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Salary findByPaymentStatus(String paymentStatus) {
        requireNonNull(paymentStatus, "payment status cannot be null");
        LOGGER.info("Retrieving salary by payment status, paymentStatus = {}", paymentStatus);
        return salaryRepository.findByPaymentStatus(valueOf(paymentStatus))
                .orElseThrow(RecordNotFoundException::new);
    }


    @Override
    public Page<Salary> findAllSalaries(Pageable pageable) {
        return salaryRepository.findAll(pageable);
    }

    @Override
    public Salary editSalary(Long salaryId,SalaryUpdateRequest updateRequest) {
        requireNonNull(salaryId, "salary id cannot be null");
        requireNonNull(updateRequest, "salary update request cannot be null");
        LOGGER.info("Update salary, salaryId = {}, updateRequest = {}", salaryId, updateRequest);
        Salary salary = findSalaryById(salaryId);
        salary.setBasicAmount(updateRequest.getBasicAmount());
        salary.setAllowanceStatus(updateRequest.isAllowance());
        salary.setPaymentStatus(valueOf(updateRequest.getPaymentStatus()));
        return salaryRepository.save(salary);
    }
}
