package zw.co.afrosoft.pensions.payroll.remuneration;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/24/20
 * 8:29 PM
 **/

public interface RemunerationService {
    Remuneration createRemuneration(RemunerationRequest request);

    Remuneration findRemunerationById(Long remunerationId);

    Page<Remuneration> findAllRemunerations(Pageable pageable);

}
