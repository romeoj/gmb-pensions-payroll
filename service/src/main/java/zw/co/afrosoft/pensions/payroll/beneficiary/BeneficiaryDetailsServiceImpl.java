package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.bank.BankingDetailsService;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import java.time.LocalDate;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @Author Romeo J
 * 8/23/20
 * 9:53 PM
 **/

@Service
public class BeneficiaryDetailsServiceImpl implements BeneficiaryDetailsService {
    private static final Logger LOGGER = getLogger(BeneficiaryDetailsServiceImpl.class);
    private final BeneficiaryDetailsRepository beneficiaryDetailsRepository;
    private final BeneficiaryService beneficiaryService;
    private final BankingDetailsService bankingDetailsService;

    public BeneficiaryDetailsServiceImpl(BeneficiaryDetailsRepository beneficiaryDetailsRepository, BeneficiaryService beneficiaryService,
                                         BankingDetailsService bankingDetailsService) {
        this.beneficiaryDetailsRepository = beneficiaryDetailsRepository;
        this.beneficiaryService = beneficiaryService;
        this.bankingDetailsService = bankingDetailsService;
    }

    @Override
    public BeneficiaryDetails createBeneficiaryDetails(BeneficiaryDetailsRequest createRequest) {
        requireNonNull(createRequest, "beneficiary details create request cannot be null");
        LOGGER.info("Create beneficiary details, createRequest = {}", createRequest);
        BeneficiaryDetails beneficiaryDetails = BeneficiaryDetails.of(createRequest);
        beneficiaryDetails.setBankingDetails(bankingDetailsService.findBankingDetailsById(
                createRequest.getBankingDetailsId()));
        beneficiaryDetails.setBeneficiary(beneficiaryService.findBeneficiaryById(createRequest.getBeneficiaryId()));
        return beneficiaryDetailsRepository.save(beneficiaryDetails);
    }

    @Override
    public BeneficiaryDetails findBeneficiaryDetailsById(Long beneficiaryDetailsId) {
        requireNonNull(beneficiaryDetailsId, "beneficiary details id cannot be null");
        LOGGER.info("Retrieving beneficiary details by id, beneficiaryDetailsId = {}", beneficiaryDetailsId);
        return beneficiaryDetailsRepository.findById(beneficiaryDetailsId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public BeneficiaryDetails editBeneficiaryDetails(Long beneficiaryDetailsId, BeneficiaryDetailsRequest createRequest) {
        requireNonNull(beneficiaryDetailsId, "beneficiary details id cannot be null");
        requireNonNull(createRequest, "beneficiary update request cannot be null");
        BeneficiaryDetails beneficiaryDetails = findBeneficiaryDetailsById(beneficiaryDetailsId);
        beneficiaryDetails.setFullName(createRequest.getFullName());
        beneficiaryDetails.setDateOfBirth(LocalDate.parse(createRequest.getDateOfBirth()));
        beneficiaryDetails.setBeneficiary(beneficiaryService.findBeneficiaryById(createRequest.getBeneficiaryId()));
        beneficiaryDetails.setNationalId(createRequest.getNationalId());
        beneficiaryDetails.setBankingDetails(bankingDetailsService.findBankingDetailsById(createRequest.getBankingDetailsId()));
        return beneficiaryDetailsRepository.save(beneficiaryDetails);
    }

    @Override
    public Page<BeneficiaryDetails> findAllBeneficiaryDetails(Pageable pageable) {
        LOGGER.info("Retrieving all beneficiary details");
        return beneficiaryDetailsRepository.findAll(pageable);
    }
}
