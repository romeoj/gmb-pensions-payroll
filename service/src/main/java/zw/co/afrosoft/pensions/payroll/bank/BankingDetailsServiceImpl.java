package zw.co.afrosoft.pensions.payroll.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;
import zw.co.afrosoft.pensions.payroll.pensioner.Pensioner;
import zw.co.afrosoft.pensions.payroll.pensioner.PensionerService;

import static java.util.Objects.requireNonNull;

/**
 * @Author Romeo J
 * 8/22/20
 * 12:21 PM
 **/

@Service
public class BankingDetailsServiceImpl  implements BankingDetailsService{
    private static final Logger LOGGER = LoggerFactory.getLogger(BankingDetailsServiceImpl.class);
    private  final BankingDetailsRepository bankingDetailsRepository;
    private final BankService bankService;
    private final PensionerService pensionerService;

    public BankingDetailsServiceImpl(BankingDetailsRepository bankingDetailsRepository,
                                     BankService bankService, PensionerService pensionerService) {
        this.bankingDetailsRepository = bankingDetailsRepository;
        this.bankService = bankService;
        this.pensionerService = pensionerService;
    }

    @Override
    public BankingDetails createBankingDetails(BankingDetailsRequest request) {
        requireNonNull(request, "Banking details request cannot be null");
        LOGGER.info("Create banking details request, request = {}", request);
        BankingDetails bankingDetails = BankingDetails.of(request);
        bankingDetails.setBank(bankService.findBankById(request.getBankId()));
        bankingDetails.setPensioner(pensionerService.findPensionerByPensionerNumber(request.getPensionerNumber()));
        return bankingDetailsRepository.save(bankingDetails);
    }

    @Override
    public BankingDetails findBankingDetailsById(Long bankingDetailsId) {
        requireNonNull(bankingDetailsId, "banking details id cannot be null");
        LOGGER.info("Retrieving banking details by id, bankingDetailsId = {}", bankingDetailsId);
        return bankingDetailsRepository.findById(bankingDetailsId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public BankingDetails findBankingDetailsByAccountNumber(String accountNumber) {
        requireNonNull(accountNumber, "banking details accountNumber cannot be null");
        LOGGER.info("Retrieving banking details by accountNumber, accountNumber = {}", accountNumber);
        return bankingDetailsRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public BankingDetails editBankingDetails(Long bankingDetailsId, BankingDetailsUpdateRequest updateRequest) {
        requireNonNull(bankingDetailsId, "banking details id cannot be null");
        requireNonNull(updateRequest, "banking details updateRequest cannot be null");
        LOGGER.info("Updating banking details , bankingDetailsId = {}, updateRequest = {}", bankingDetailsId, updateRequest);
        BankingDetails bankingDetails = findBankingDetailsById(bankingDetailsId);
        Bank bank = bankService.findBankById(updateRequest.getBankId());
        Pensioner pensioner = pensionerService.findPensionerByPensionerNumber(updateRequest.getPensionerNumber());
        requireNonNull(bank, "bank cannot be null");
        requireNonNull(pensioner, "pensioner cannot be null");
        bankingDetails.setBank(bank);
        bankingDetails.setPensioner(pensioner);
        return bankingDetailsRepository.save(bankingDetails);
    }

    @Override
    public Page<BankingDetails> findAllBankingDetails(Pageable pageable) {
        return bankingDetailsRepository.findAll(pageable);
    }
}
