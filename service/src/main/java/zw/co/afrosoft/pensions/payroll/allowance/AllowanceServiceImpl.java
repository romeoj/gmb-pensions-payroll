package zw.co.afrosoft.pensions.payroll.allowance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;

import static java.util.Objects.requireNonNull;
import static zw.co.afrosoft.pensions.payroll.allowance.Allowance.of;
import static zw.co.afrosoft.pensions.payroll.enums.Category.valueOf;

/**
 * @Author Romeo J
 * 8/22/20
 * 9:43 AM
 **/

@Service
public class AllowanceServiceImpl implements AllowanceService{
    private static final Logger LOGGER = LoggerFactory.getLogger(AllowanceServiceImpl.class);
    private final AllowanceRepository allowanceRepository;

    public AllowanceServiceImpl(AllowanceRepository allowanceRepository) {
        this.allowanceRepository = allowanceRepository;
    }

    @Override
    public Allowance createAllowance(AllowanceCreateRequest request) {
        requireNonNull(request, "Allowance create request cannot be null");
        LOGGER.info("Create allowance, request = {}", request);
        Allowance allowance = of(request);
        allowance.setCategory(valueOf(request.getCategory()));
        return allowanceRepository.save(allowance);
    }

    @Override
    public Allowance findAllowanceById(Long allowanceId) {
        requireNonNull(allowanceId, "allowance id cannot be null");
        LOGGER.info("Retrieving allowance by id, allowanceId = {}", allowanceId);
        return allowanceRepository.findById(allowanceId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Allowance findByIsActive(Boolean isActive) {
        requireNonNull(isActive, "allowance active cannot be null");
        LOGGER.info("Retrieving allowance by active, isActive = {}", isActive);
        return allowanceRepository.findByIsActive(isActive).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Allowance findByIsTaxable(Boolean isTaxable) {
        requireNonNull(isTaxable, "allowance taxable cannot be null");
        LOGGER.info("Retrieving allowance by taxable, isTaxable = {}", isTaxable);
        return allowanceRepository.findByIsTaxable(isTaxable).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Allowance findByAllowanceName(String allowanceName) {
        requireNonNull(allowanceName, "allowance name cannot be null");
        LOGGER.info("Retrieving allowance by name, allowanceName = {}", allowanceName);
        return allowanceRepository.findByAllowanceName(allowanceName).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Page<Allowance> findAllAllowances(Pageable pageable) {
        LOGGER.info("Retrieving all allowances");
        return allowanceRepository.findAll(pageable);
    }

    @Override
    public Allowance editAllowance(Long allowanceId, AllowanceUpdateRequest updateRequest) {
        requireNonNull(allowanceId, "allowance id cannot be null");
        requireNonNull(updateRequest, "allowance update request cannot be null");
        LOGGER.info("Updating allowance, allowanceId = {}, updateRequest = {}", allowanceId, updateRequest);
        Allowance allowance = findAllowanceById(allowanceId);
        allowance.setFixedAmount(updateRequest.getFixedAmount());
        allowance.setAllowanceName(updateRequest.getAllowanceName());
        allowance.setTaxPercentage(updateRequest.getTaxPercentage());
        allowance.setCategory(valueOf(updateRequest.getCategory()));
        allowance.setIsFixed(updateRequest.isFixed());
        allowance.setIsActive(updateRequest.isActive());
        allowance.setIsTaxable(updateRequest.isTaxable());
        return allowanceRepository.save(allowance);
    }
}
