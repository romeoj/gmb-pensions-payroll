package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author Romeo J
 * 8/23/20
 * 9:50 PM
 **/

public interface BeneficiaryDetailsService {
    BeneficiaryDetails createBeneficiaryDetails(BeneficiaryDetailsRequest createRequest);

    BeneficiaryDetails findBeneficiaryDetailsById(Long id);

    BeneficiaryDetails editBeneficiaryDetails(Long beneficiaryDetailsId, BeneficiaryDetailsRequest request);

    Page<BeneficiaryDetails> findAllBeneficiaryDetails(Pageable pageable);

}
