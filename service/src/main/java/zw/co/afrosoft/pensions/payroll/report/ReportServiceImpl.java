package zw.co.afrosoft.pensions.payroll.report;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * @Author Romeo J
 * 8/25/20
 * 12:40 PM
 **/

@Service
public class ReportServiceImpl implements ReportService{
    private DataSource dataSource;

    @Autowired
    public ReportServiceImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public String generateReport(String reportName, Map<String, Object> params, HttpServletResponse response) throws
            IOException, JRException, SQLException {
        InputStream jasperStream = this.getClass().getResourceAsStream("/reports/" + reportName + ".jasper");

        BufferedImage logo =
                ImageIO.read(this.getClass().getResource("/reports/icon/logo.png"));
        params.put("logo", logo);
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
        Connection con = dataSource.getConnection();
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, con);
        response.setContentType("application/pdf");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "inline; filename=" + reportName + ".pdf");
        final OutputStream outStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
        response.getOutputStream().flush();
        response.getOutputStream().close();
        con.close();
        return "";
    }
}
