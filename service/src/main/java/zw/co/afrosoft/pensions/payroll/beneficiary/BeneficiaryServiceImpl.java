package zw.co.afrosoft.pensions.payroll.beneficiary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.pensions.payroll.enums.Relationship;
import zw.co.afrosoft.pensions.payroll.exception.RecordNotFoundException;
import zw.co.afrosoft.pensions.payroll.pensioner.PensionerService;

import static java.util.Objects.requireNonNull;

/**
 * @Author Romeo J
 * 8/23/20
 * 10:44 AM
 **/
@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BeneficiaryServiceImpl.class);
    private final BeneficiaryRepository beneficiaryRepository;
    private final PensionerService pensionerService;

    public BeneficiaryServiceImpl(BeneficiaryRepository beneficiaryRepository,
                                  PensionerService pensionerService) {
        this.beneficiaryRepository = beneficiaryRepository;
        this.pensionerService = pensionerService;
    }

    @Override
    public Beneficiary createBeneficiary(BeneficiaryCreateRequest createRequest) {
        requireNonNull(createRequest, "beneficiary create request cannot be null");
        LOGGER.info("Create beneficiary, createRequest = {}", createRequest);
        Beneficiary beneficiary = Beneficiary.of(createRequest);
        beneficiary.setPensioner(pensionerService.findPensionerByPensionerNumber(createRequest.getPensionerNumber()));
        beneficiary.setRelationship(Relationship.valueOf(createRequest.getRelationship()));
        return beneficiaryRepository.save(beneficiary);
    }

    @Override
    public Beneficiary findBeneficiaryById(Long beneficiaryId) {
        requireNonNull(beneficiaryId, "beneficiary id cannot be null");
        LOGGER.info("Retrieve beneficiary by id, beneficiaryId = {}", beneficiaryId);
        return beneficiaryRepository.findById(beneficiaryId).orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Page<Beneficiary> findAllBeneficiaries(Pageable pageable) {
        LOGGER.info("Retrieving all beneficiaries");
        return beneficiaryRepository.findAll(pageable);
    }

    @Override
    public Beneficiary editBeneficiary(Long beneficiaryId, BeneficiaryUpdateRequest updateRequest) {
        requireNonNull(beneficiaryId, "beneficiary id cannot be null");
        requireNonNull(updateRequest, "beneficiary update request cannot be null");
        LOGGER.info("Update beneficiary, beneficiaryId = {}, updateRequest = {}", beneficiaryId, updateRequest);
        Beneficiary beneficiary = findBeneficiaryById(beneficiaryId);
        beneficiary.setNominee(updateRequest.isNominee());
        beneficiary.setBeneficiary(updateRequest.isBeneficiary());
        return beneficiaryRepository.save(beneficiary);
    }
}
